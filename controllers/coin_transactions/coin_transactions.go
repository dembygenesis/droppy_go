package coin_transactions

import (
	CoinTransactionsModel "droppy_go/models_app/coin_transactions"
	ResponseBuilder "droppy_go/utilities/response_builder"
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"strconv"
)

func GetAllOld(c *fiber.Ctx) error {
	t := CoinTransactionsModel.CoinTransaction{}

	// Attempt to fetch all transactions
	res, err := t.GetAll()

	fmt.Println(res)

	if err != nil {
		r := ResponseBuilder.Response{}
		r.HttpCode = 200
		r.ResponseMessage = "Something went wrong when trying to fetch the coin transactions"
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Here's the coin transactions"
	r.SetResponseData(res)

	return c.JSON(r)
}

func GetAll(c *fiber.Ctx) error {
	t := CoinTransactionsModel.CoinTransaction{}

	/**
	Pagination
	*/
	var page int
	var rows int

	// Set default rows to 100 if not paginated
	if c.Query("page") == "" {
		page = 0
	} else {
		page, _ = strconv.Atoi(c.Query("page"))
	}

	if c.Query("rows") == "" {
		rows = 100
	} else {
		rows, _ = strconv.Atoi(c.Query("rows"))

		if rows <= 0 {
			rows = 1000
		}
	}

	/**
	Filters
	*/
	var filters []string

	f := c.Query("search")

	if f != "" {
		err := json.Unmarshal([]byte(f), &filters)

		if err != nil {
			r := ResponseBuilder.Response{}
			r.HttpCode = 200
			r.ResponseMessage = "Something went wrong when trying to decode the filters"
			r.AddErrors("Something went wrong when trying to decode the filters " + err.Error() + "with value of : " + f)

			return c.JSON(r)
		}
	}

	startDate := c.Query("start")
	endDate := c.Query("end")

	// Attempt to fetch all transactions
	res, pagination, err := t.GetAll2(page, rows, filters, startDate, endDate)

	if err != nil {
		r := ResponseBuilder.Response{}
		r.HttpCode = 200
		r.ResponseMessage = "Something went wrong when trying to fetch the coin transactions"
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Here's the coin transactions"
	r.Pagination = pagination
	r.SetResponseData(res)

	return c.JSON(r)
}

func Create(c *fiber.Ctx) error {

	return nil
	/*transaction := CoinTransactionsModel.Transaction{}

	res, err := transaction.Create()

	if err != nil {
		r := ResponseBuilder.Response{
			HttpCode: 200,
			ResponseMessage: "Something went wrong when trying to insert a transaction",
		}
		r.AddErrors(err.Error())

		c.JSON(r)

		return
	}

	r := ResponseBuilder.Response{
		HttpCode: 200,
		ResponseMessage: "Successfully added a new transaction",
	}
	r.SetResponseData(res)

	c.JSON(r)*/
}