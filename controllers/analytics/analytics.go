package analytics

import (
	"droppy_go/services/analytics"
	"droppy_go/utilities/response_builder"
	"github.com/gofiber/fiber/v2"
)

func GetAcceptancePercentages(c *fiber.Ctx) error {

	startDate := c.Query("start_date")
	endDate := c.Query("end_date")
	userId := c.Locals("tokenExtractedUserId").(int)

	var missingParameters []string

	if startDate == "" {
		missingParameters = append(missingParameters, "start_date is empty")
	}

	if endDate == "" {
		missingParameters = append(missingParameters, "end_date is empty")
	}

	if userId == 0 {
		missingParameters = append(missingParameters, "user_id is invalid/empty")
	}

	if len(missingParameters) > 0 {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the acceptance percentages",
		}
		r.SetErrors(missingParameters)

		return c.JSON(r)
	}

	res, err := analytics.GetAcceptancePercentages(startDate, endDate, userId)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the acceptance percentages",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := response_builder.Response{
		HttpCode:        200,
		ResponseMessage: "Successfully queried the acceptance percentages",
	}
	r.SetResponseData(res)

	return c.JSON(r)
}

func GetLineChart(c *fiber.Ctx) error {

	startDate := c.Query("start_date")
	endDate := c.Query("end_date")
	userId := c.Locals("tokenExtractedUserId").(int)

	var missingParameters []string

	if startDate == "" {
		missingParameters = append(missingParameters, "start_date is empty")
	}

	if endDate == "" {
		missingParameters = append(missingParameters, "end_date is empty")
	}

	if userId == 0 {
		missingParameters = append(missingParameters, "user_id is invalid/empty")
	}

	if len(missingParameters) > 0 {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the analytics",
		}
		r.SetErrors(missingParameters)

		return c.JSON(r)
	}

	res, err := analytics.GetDeliveryLineItems(startDate, endDate, userId)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the analytics",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := response_builder.Response{
		HttpCode:        200,
		ResponseMessage: "Successfully queried the analytics",
	}
	r.SetResponseData(res)

	return c.JSON(r)
}

func GetLineChartTotalSales(c *fiber.Ctx) error {

	userId := c.Locals("tokenExtractedUserId").(int)
	userType := c.Locals("tokenExtractedUserType").(string)

	var missingParameters []string

	if userId == 0 {
		missingParameters = append(missingParameters, "user_id is invalid/empty")
	}

	if len(missingParameters) > 0 {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the total sales line chart data",
		}
		r.SetErrors(missingParameters)

		return c.JSON(r)
	}

	res, err := analytics.GetLineChartTotalSales(userId, userType)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the total sales line chart data",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := response_builder.Response{
		HttpCode:        200,
		ResponseMessage: "Successfully queried the total sales line chart data",
	}

	r.SetResponseData(res)

	return c.JSON(r)
}

func GetPieChart(c *fiber.Ctx) error {

	startDate := c.Query("start_date")
	endDate := c.Query("end_date")
	userId := c.Locals("tokenExtractedUserId").(int)

	var missingParameters []string

	if startDate == "" {
		missingParameters = append(missingParameters, "start_date is empty")
	}

	if endDate == "" {
		missingParameters = append(missingParameters, "end_date is empty")
	}

	if userId == 0 {
		missingParameters = append(missingParameters, "user_id is invalid/empty")
	}

	if len(missingParameters) > 0 {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the analytics",
		}
		r.SetErrors(missingParameters)

		return c.JSON(r)
	}

	res, err := analytics.GetDeliveryPieItems(startDate, endDate, userId)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the analytics",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := response_builder.Response{
		HttpCode:        200,
		ResponseMessage: "Successfully queried the analytics",
	}
	r.SetResponseData(res)

	return c.JSON(r)
}