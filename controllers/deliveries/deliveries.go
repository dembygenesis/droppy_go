package deliveries

import (
	DeliveryModel "droppy_go/models_app/deliveries"
	"droppy_go/utilities/response_builder"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"strconv"

	ResponseBuilder "droppy_go/utilities/response_builder"
)

func ServiceFee(c *fiber.Ctx) error {
	deliveryId, _ := strconv.Atoi(c.Params("id"))

	u := DeliveryModel.Delivery{ID: deliveryId}

	orderDetails := c.Query("order_details")

	res, err := u.GetServiceFee(orderDetails)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the service fee",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Here's your service fee"
	r.SetResponseData(res)

	return c.JSON(r)
}

func Get(c *fiber.Ctx) error {
	deliveryId, _ := strconv.Atoi(c.Params("id"))

	u := DeliveryModel.Delivery{ID: deliveryId}

	userId := c.Locals("tokenExtractedUserId").(int)
	userType := c.Locals("tokenExtractedUserType").(string)

	res, err := u.GetDeliveryDetails(userId, userType)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the delivery details",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Here's your delivery details"
	r.SetResponseData(res)

	return c.JSON(r)
}

func Top10Sellers(c *fiber.Ctx) error {
	u := DeliveryModel.Delivery{}

	userId := c.Locals("tokenExtractedUserId").(int)
	userType := c.Locals("tokenExtractedUserType").(string)

	res, err := u.GetDashboardDeliveryStatus(userId, userType)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the orders",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Here's your store details"
	r.SetResponseData(res)

	return c.JSON(r)
}

func GetCouriers(c *fiber.Ctx) error {
	d := DeliveryModel.Delivery{}

	res, err := d.GetCouriers()
	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the orders",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Here's your delivery couriers"
	r.SetResponseData(res)

	return c.JSON(r)
}

func MyStore(c *fiber.Ctx) error {
	u := DeliveryModel.Delivery{}

	userId := c.Locals("tokenExtractedUserId").(int)
	userType := c.Locals("tokenExtractedUserType").(string)

	res, err := u.GetDashboardDeliveryStatus(userId, userType)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the orders",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Here's your store details"
	r.SetResponseData(res)

	return c.JSON(r)
}

// MARKER
func Rates(c *fiber.Ctx) error {
	// Hello
	d := DeliveryModel.Delivery{}

	courierId := c.Query("courier_id")
	declaredAmount := c.Query("declared_amount")

	parsedCourierId, err := strconv.Atoi(courierId)
	if err != nil {

	}
	parsedDeclaredAmount, err := strconv.ParseFloat(declaredAmount, 64)
	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to create your parcel",
		}
		r.AddErrors("error parsing the declared amount: " + err.Error())
		r.AddErrors(err.Error())

		return c.JSON(r)
	}
	rate, err := d.GetRates(parsedCourierId, parsedDeclaredAmount)
	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to create your parcel",
		}
		r.AddErrors("Something went wrong when parsing the body for UpdateDelivery: " + err.Error())
		return c.JSON(r)
	}

	r := response_builder.Response{
		HttpCode:        200,
		ResponseMessage: "Here's your rate",
		OperationStatus: "FETCH_SUCCESS",
		Data: rate,
	}

	return c.JSON(r)
}

func UpdateDelivery(c *fiber.Ctx) error {
	u := DeliveryModel.Delivery{}

	var paramsUpdateDelivery DeliveryModel.ParamsUpdateDelivery
	err := c.BodyParser(&paramsUpdateDelivery)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to create your parcel",
		}
		r.AddErrors("Something went wrong when parsing the body for UpdateDelivery")
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	// Guard against param errors the GO way lol
	var paramErrors []string

	/**
	DeliveryId
	DeliveryStatus
	TrackingNumber
	 */

	if paramsUpdateDelivery.DeliveryId == 0 {
		paramErrors = append(paramErrors, "delivery_id empty")
	}

	if paramsUpdateDelivery.DeliveryStatus == "" {
		paramErrors = append(paramErrors, "delivery_status empty")
	}

	// Nah ignore this, errors will be caught by the backend anyway
	if paramsUpdateDelivery.TrackingNumber == "" {
		// paramErrors = append(paramErrors, "declared_amount empty")
	}

	if len(paramErrors) > 0 {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to update your delivery",
		}
		r.SetErrors(paramErrors)

		return c.JSON(r)
	}

	userId := c.Locals("tokenExtractedUserId").(int)

	u.UserId = userId

	_, err = u.UpdateDelivery(paramsUpdateDelivery)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to create your parcel",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := response_builder.Response{
		HttpCode:        200,
		ResponseMessage: "Successfully updated the delivery",
		OperationStatus: "UPDATE_SUCCESS",
	}

	return c.JSON(r)
}

func OrderParcel(c *fiber.Ctx) error {
	u := DeliveryModel.Delivery{}
	userId := c.Locals("tokenExtractedUserId").(int)
	var p DeliveryModel.ParamsCreateParcel

	err := c.BodyParser(&p)
	if p.Valid() == false {
		e := p.GetInputErrors()
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to create your parcel",
		}
		r.SetErrors(e)
		return c.JSON(r)
	}

	p.SellerId = userId

	res, err := u.CreateParcel(&p)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to create your parcel",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Successfully Created Your Parcel!"
	r.OperationStatus = "INSERT_SUCCESS"
	r.SetResponseData(res)

	return c.JSON(r)
}

func OrderPackage(c *fiber.Ctx) error {
	u := DeliveryModel.Delivery{}

	userId := c.Locals("tokenExtractedUserId").(int)
	// userType := c.Locals("tokenExtractedUserType").(string)

	var paramsCreateOrder DeliveryModel.ParamsCreateOrder

	err := c.BodyParser(&paramsCreateOrder)

	paramsCreateOrder.SellerId = userId

	res, err := u.CreatePackage(paramsCreateOrder)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to create your order",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Successfully Created Your Order!"
	r.OperationStatus = "INSERT_SUCCESS"
	r.SetResponseData(res)

	return c.JSON(r)
}

func CoinTransactions(c *fiber.Ctx) error {
	u := DeliveryModel.Delivery{}

	userId := c.Locals("tokenExtractedUserId").(int)
	userType := c.Locals("tokenExtractedUserType").(string)

	/**
	Pagination
	*/
	var page int
	var rows int

	// Set default rows to 100 if not paginated
	if c.Query("page") == "" {
		page = 0
	} else {
		page, _ = strconv.Atoi(c.Query("page"))
	}

	if c.Query("rows") == "" {
		rows = 100
	} else {
		rows, _ = strconv.Atoi(c.Query("rows"))

		if rows <= 0 {
			rows = 1000
		}
	}

	res, pagination, err := u.GetCoinTransactions(userId, userType, page, rows)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the coin transactions",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Here's your coin transaction details"
	r.SetResponseData(res)
	r.Pagination = pagination

	return c.JSON(r)
}

func CoinTransactions2(c *fiber.Ctx) error {
	u := DeliveryModel.Delivery{}

	userId := c.Locals("tokenExtractedUserId").(int)
	userType := c.Locals("tokenExtractedUserType").(string)

	/**
	Pagination
	*/
	var page int
	var rows int

	// Set default rows to 100 if not paginated
	if c.Query("page") == "" {
		page = 0
	} else {
		page, _ = strconv.Atoi(c.Query("page"))
	}

	if c.Query("rows") == "" {
		rows = 100
	} else {
		rows, _ = strconv.Atoi(c.Query("rows"))

		if rows <= 0 {
			rows = 1000
		}
	}

	res, pagination, err := u.GetCoinTransactions2(userId, userType, page, rows)

	fmt.Println("CoinTransactions err", err)

	if err != nil {
		fmt.Println("Here I AM")
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the coin transactions",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)
	} else {
		fmt.Println("Here I AM else", err)
	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Here's your coin transaction details"
	r.SetResponseData(res)
	r.Pagination = pagination

	return c.JSON(r)
}

func Transactions(c *fiber.Ctx) error {
	u := DeliveryModel.Delivery{}

	userId := c.Locals("tokenExtractedUserId").(int)
	userType := c.Locals("tokenExtractedUserType").(string)

	/**
		Pagination
	 */
	var page int
	var rows int

	// Set default rows to 100 if not paginated
	if c.Query("page") == "" {
		page = 0
	} else {
		page, _ = strconv.Atoi(c.Query("page"))
	}

	if c.Query("rows") == "" {
		rows = 100
	} else {
		rows, _ = strconv.Atoi(c.Query("rows"))

		if rows <= 0 {
			rows = 1000
		}
	}

	/**
		Search
	*/
	var search string

	if c.Query("search") == "" {
		search = ""
	} else {
		search = c.Query("search")
	}

	deliveryStatus := c.Query("delivery_status")

	res, pagination, err := u.GetTransactions(userId, userType, deliveryStatus, search, page, rows)

	if err != nil {
		r := response_builder.Response{
			HttpCode:        200,
			ResponseMessage: "Failed to fetch the orders",
		}
		r.AddErrors(err.Error())

		return c.JSON(r)

	}

	r := ResponseBuilder.Response{}
	r.HttpCode = 200
	r.ResponseMessage = "Here's your store details"
	r.SetResponseData(res)
	r.Pagination = pagination

	return c.JSON(r)
}