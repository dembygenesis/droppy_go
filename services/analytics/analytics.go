package analytics

import (
	"droppy_go/database"
	ModelAnalytics "droppy_go/models_app/analytics"
)

func GetAcceptancePercentages(s string, e string, i int) ([]ModelAnalytics.AcceptedPercentages, error) {
	var acceptedPercentages []ModelAnalytics.AcceptedPercentages

	sql := `
		SELECT
		  IF(total_orders IS NULL, 0, total_orders) AS total_orders,
		  IF(delivered / total_orders IS NULL, 0, delivered / total_orders) AS delivered,
		  IF(undelivered / total_orders IS NULL, 0, undelivered / total_orders) AS undelivered,
		  IF(returned / total_orders IS NULL, 0, returned / total_orders) AS returned
		FROM
		  (SELECT
			COUNT(*) AS total_orders,
			COUNT(
			  IF(
				ds.name = 'Returned',
				ds.id,
				NULL
			  )
			) AS returned,
			COUNT(
			  IF(
				ds.name IN ('Fulfilled', 'Accepted'),
				ds.id,
				NULL
			  )
			) AS undelivered,
			COUNT(
			  IF(
				ds.name = 'Delivered',
				ds.id,
				NULL
			  )
			) AS delivered
		  FROM
			delivery d
			INNER JOIN delivery_status ds
			  ON 1 = 1
			  AND d.delivery_status_id = ds.id
		  WHERE 1 = 1
			AND d.is_active = 1
			AND d.created_date BETWEEN ? AND ?
		    AND d.seller_id = ?
			AND ds.name NOT IN ('Proposed', 'Voided', 'Rejected')) AS a
	`
	err := database.DBInstancePublic.Select(&acceptedPercentages, sql, s, e, i)

	return acceptedPercentages, err
}

func GetDeliveryLineItems(s string, e string, i int) ([]ModelAnalytics.DeliveryLineChartItem, error) {
	var deliveryLineChartItems []ModelAnalytics.DeliveryLineChartItem

	sql := `
		SELECT
		  COUNT(*) AS delivery_count,
		  DATE_FORMAT(d.created_date, '%b %d') AS delivery_date
		FROM delivery d
		INNER JOIN delivery_status ds 
			ON 1 = 1
		      AND d.delivery_status_id = ds.id
		WHERE 1 = 1
		  AND d.is_active = 1
		  AND d.created_date BETWEEN ? AND ?
		  AND d.seller_id = ?
		  AND ds.name NOT IN ('Proposed', 'Voided')
		  GROUP BY DATE_FORMAT(d.created_date, '%Y-%m-%d')
		  ORDER BY d.created_date ASC
	`
	err := database.DBInstancePublic.Select(&deliveryLineChartItems, sql, s, e, i)

	return deliveryLineChartItems, err
}

func GetLineChartTotalSales(i int, ut string) ([]ModelAnalytics.LineChartTotalSalesItem, error) {
	var lineChartTotalSalesItem []ModelAnalytics.LineChartTotalSalesItem

	var sql string
	var err error

	if ut == "Seller" {
		sql = `
			SELECT
			  DATE_FORMAT(d.created_date, '%b %d') AS month,
			  SUM(d.declared_amount) AS amount
			FROM
			  delivery d
			WHERE 1 = 1
			  AND d.seller_id = ?
			  AND d.created_date BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND NOW()
			GROUP BY DATE_FORMAT(d.created_date, '%b %d')
			ORDER BY d.created_date ASC
		`

		err = database.DBInstancePublic.Select(&lineChartTotalSalesItem, sql, i)
	} else if ut == "Dropshipper" {
		sql = `
			SELECT
			  DATE_FORMAT(d.created_date, '%b %d') AS month,
			  SUM(d.declared_amount) AS amount
			FROM
			  delivery d
			WHERE 1 = 1
			  AND d.dropshipper = ?
			  AND d.created_date BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND NOW()
			GROUP BY DATE_FORMAT(d.created_date, '%b %d')
			ORDER BY d.created_date ASC
		`

		err = database.DBInstancePublic.Select(&lineChartTotalSalesItem, sql, i)
	} else {
		sql = `
			SELECT
			  DATE_FORMAT(d.created_date, '%b %d') AS month,
			  SUM(d.declared_amount) AS amount
			FROM
			  delivery d
			WHERE 1 = 1
			  AND d.created_date BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND NOW()
			GROUP BY DATE_FORMAT(d.created_date, '%b %d')
			ORDER BY d.created_date ASC
		`

		err = database.DBInstancePublic.Select(&lineChartTotalSalesItem, sql, i)
	}

	return lineChartTotalSalesItem, err
}

func GetDeliveryPieItems(s string, e string, i int) ([]ModelAnalytics.DeliveryPieChartItem, error) {
	var deliveryPieChartItems []ModelAnalytics.DeliveryPieChartItem

	sql := `
		SELECT
		  CASE 
		    WHEN ds.name = 'Delivered' THEN 'Delivered'
		    WHEN ds.name = 'Returned' THEN 'Returned'
		    ELSE 'Undelivered'
		  END AS delivery_type,    
		  COUNT(*) AS delivery_count
		FROM
		  delivery d
		  INNER JOIN delivery_status ds
			ON 1= 1
			  AND d.delivery_status_id = ds.id
		WHERE 1 = 1
		  AND d.is_active = 1
		  AND d.created_date BETWEEN ? AND ?
		  AND d.seller_id = ?
		  AND ds.name NOT LIKE 'Same%'
		  AND ds.name != 'Rejected'
		GROUP BY delivery_type
		ORDER BY d.created_date ASC
	`
	err := database.DBInstancePublic.Select(&deliveryPieChartItems, sql, s, e, i)

	return deliveryPieChartItems, err
}