package users

import (
	"droppy_go/database"
	// "errors"
	"errors"
	"fmt"

	UserModel "droppy_go/models_app/users"

	// "fmt"

	StringUtility "droppy_go/utilities/string"
)

func GetAll() (*[]UserModel.UserListDisplay, error) {
	user := UserModel.User{}

	res, err := user.GetAll()

	return res, err
}

// Returns a token if successful, error if not lol
func Login(email string, password string) (string, UserModel.ResponseLoginUserInfo, error) {

	var responseLoginUserInfo UserModel.ResponseLoginUserInfo

	var jwtToken = ""

	user := UserModel.User{Email: email, Password: password}

	// Get password via email
	password, id, err := user.GetPasswordAndIdByEmail()

	if err != nil {
		return jwtToken, responseLoginUserInfo, errors.New("something went wrong when trying to check the password")
	}

	// Attempt to match password
	matched := StringUtility.Decrypt(password, user.Password)

	if matched == false {
		return jwtToken, responseLoginUserInfo, errors.New("username/password match failed")
	}

	// Extract JWT
	jwtToken, err = StringUtility.MakeJWT(id)

	if err != nil {
		return jwtToken, responseLoginUserInfo, errors.New("something went wrong when trying to extract a JWTToken")
	}

	// Get login details
	responseLoginUserInfo, err = user.GetLoginDetails()

	if err != nil {
		fmt.Println("hhohoh", err)
		// return jwtToken, responseLoginUserInfo, errors.New("something went wrong when trying to get report login user info")
		fmt.Println("password", password)
		return jwtToken, responseLoginUserInfo, err
	}

	fmt.Print(user)

	return jwtToken, responseLoginUserInfo, nil
}


type UserDetail struct {
	Id        int    `json:"id" db:"id"`
	FirstName string `json:"firstname" db:"firstname"`
	LastName  string `json:"lastname" db:"lastname"`
	Email     string `json:"email" db:"email"`
}

func GetUserDetails(id int) (*UserDetail, error) {
	var userDetail []UserDetail
	sql := `
		SELECT 
			u.id,
			u.firstname,
			u.lastname,
			u.email
		FROM ` + StringUtility.EncloseString("user", "`") + ` u
		WHERE 1 = 1
			AND u.id = ?
	`
	err := database.DBInstancePublic.Select(&userDetail, sql, id)
	if err != nil {
		return &userDetail[0], err
	}

	return &userDetail[0], nil
}