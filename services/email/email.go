package email

import (
	"fmt"
	"gopkg.in/gomail.v2"
	"os"
	"strconv"
)

func SendMail(
	from string,
	to string,
	subject string,
	message string,
	attachment string,
) error {
	m := gomail.NewMessage()

	m.SetHeader("From", from)
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", message)

	if attachment != "" {
		m.Attach(attachment)
	}

	emailSettings := Settings{
		Port:     os.Getenv("EMAIL_PORT"),
		Host:     os.Getenv("EMAIL_HOST"),
		Username: os.Getenv("EMAIL_USERNAME"),
		Password: os.Getenv("EMAIL_PASSWORD"),
	}

	parsedEmailPort, err := strconv.Atoi(emailSettings.Port)
	if err != nil {
		return err
	}
	d := gomail.NewDialer(emailSettings.Host, parsedEmailPort, emailSettings.Username, emailSettings.Password)

	if err = d.DialAndSend(m); err != nil {
		fmt.Println("I have an error")
		return err
	} else {
		fmt.Println("I have no error")
		return nil
	}
}