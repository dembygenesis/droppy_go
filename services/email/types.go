package email

// Settings are the credentials used for the smtp client
type Settings struct {
	Port     string
	Host     string
	Username string
	Password string
}