package file

import (
	"archive/zip"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

func GenerateFolders(f string) error {
	err := os.MkdirAll(f, os.ModePerm)

	if err != nil {
		return err
	}

	return nil
}

// Goes inside a folder and zip all files in between
func ZipFilesInFolder(folderName string, outputDir string) error {
	// Get all files here
	var filesInFolder []string

	root := folderName
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		filesInFolder = append(filesInFolder, path)
		return nil
	})
	if err != nil {
		fmt.Println("Return 1", err)
		return err
	}

	// Remove first element from array
	filesInFolder = append(filesInFolder[:0], filesInFolder[0+1:]...)

	// Zip all files found in folder
	if err := ZipFiles(outputDir, filesInFolder); err != nil {
		fmt.Println("Return 2")
		return err
	}

	fmt.Println("Return 3")
	return nil
}

// ZipFiles self explanatory
func ZipFiles(filename string, files []string) error {

	newZipFile, err := os.Create(filename)
	if err != nil {
		fmt.Println("Error creating zip filename", filename)
		return err
	}
	defer newZipFile.Close()
	// newZipFile.Close()


	zipWriter := zip.NewWriter(newZipFile)
	defer zipWriter.Close()


	// Add files to zip
	for _, file := range files {
		if err = AddFileToZip(zipWriter, file); err != nil {
			fmt.Println("Error adding file to zip", err)
			return err
		}
	}


	/*defer newZipFile.Close()
	defer zipWriter.Close()*/

	// newZipFile.Close()

	return nil
}

// AddFileToZip self explanatory
func AddFileToZip(zipWriter *zip.Writer, filename string) error {

	fileToZip, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer fileToZip.Close()
	// fileToZip.Close()

	// Get the file information
	info, err := fileToZip.Stat()
	if err != nil {
		return err
	}

	header, err := zip.FileInfoHeader(info)
	if err != nil {
		return err
	}

	// Using FileInfoHeader() above only uses the basename of the file. If we want
	// to preserve the folder structure we can overwrite this with the full path.
	header.Name = filename

	// Change to deflate to gain better compression
	// see http://golang.org/pkg/archive/zip/#pkg-constants
	header.Method = zip.Deflate

	writer, err := zipWriter.CreateHeader(header)
	if err != nil {
		return err
	}
	_, err = io.Copy(writer, fileToZip)
	return err
}

// FileExists self explanatory
func FileExists(fileName string) bool {
	if _, err := os.Stat(fileName); err == nil {
		return true
	}
	return false
}

func GetAsJSON(i interface{}) string {
	json, err := json.Marshal(i)
	if err != nil {
		return "Something went wrong when coverting to JSON"
	} else {
		return string(json)
	}
}

// Check if files exist
func fileExists(s string) (bool, error) {
	return false, nil
}
