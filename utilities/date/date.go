package date

import (
	"fmt"
	"github.com/jinzhu/now"
	"time"
)

func ValidateDate(s string) bool {
	_, err := time.Parse("2006-01-02", s)
	if err != nil {
		return false
	}

	return true
}

func GetRanges(s, e string) []DateRange {
	// Parse start
	l := "2006-01-02"
	start, err := time.Parse(l, s)
	if err != nil {
		panic(err)
	}
	start = now.New(start).BeginningOfMonth()

	// Parse end
	end, err := time.Parse(l, e)
	if err != nil {
		panic(err)
	}

	dateRanges := make([]DateRange, 0)

	// Difference
	for monthDifference(end, start) != 0 {
		fmt.Println("monthDifference(end, start)", monthDifference(end, start))
		dateRanges = append(dateRanges, DateRange{
			Start: now.New(start).BeginningOfMonth().Format(l),
			End:   now.New(start).EndOfMonth().Format(l),
		})
		start = now.New(start).EndOfMonth().Add(time.Hour * 24)
	}
	dateRanges = append(dateRanges, DateRange{
		Start: now.New(start).Format(l),
		End:   end.Format(l),
	})

	return dateRanges
}

type DateRange struct {
	Start string
	End   string
}

func monthDifference(a, b time.Time) int {
	y, mo, _, _, _, _ := diff(a, b)
	if y > 0 {
		mo = mo + (y * 12)
	}
	return mo
}

// https://golangr.com/difference-between-two-dates/
func diff(a, b time.Time) (year, month, day, hour, min, sec int) {
	if a.Location() != b.Location() {
		b = b.In(a.Location())
	}
	if a.After(b) {
		a, b = b, a
	}
	y1, M1, d1 := a.Date()
	y2, M2, d2 := b.Date()

	h1, m1, s1 := a.Clock()
	h2, m2, s2 := b.Clock()

	year = int(y2 - y1)
	month = int(M2 - M1)
	day = int(d2 - d1)
	hour = int(h2 - h1)
	min = int(m2 - m1)
	sec = int(s2 - s1)

	// Normalize negative values
	if sec < 0 {
		sec += 60
		min--
	}
	if min < 0 {
		min += 60
		hour--
	}
	if hour < 0 {
		hour += 24
		day--
	}
	if day < 0 {
		// days in month:
		t := time.Date(y1, M1, 32, 0, 0, 0, 0, time.UTC)
		day += 32 - t.Day()
		month--
	}
	if month < 0 {
		month += 12
		year--
	}

	return
}
