package database_utils

import (
	"database/sql"
	"fmt"
	"github.com/friendsofgo/errors"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"math"
	"reflect"
	"regexp"
	"strconv"
	"time"
)

// GetSQLInstance returns a database instance (MYSQL for now)
// We can make this a plug and plug utility, but not right now lol
func GetSQLInstance(
	host,
	username,
	password,
	port,
	database string,
) (*sql.DB, error) {
	connString :=
		username + ":" +
		password + "@tcp(" +
		host + ":" +
		port + ")/" +
		database + "?charset=utf8&parseTime=true"
	fmt.Println("conn str: " + connString)
	return sql.Open("mysql", connString)
}

// GetRowContentsAsMultiStringArr builds the SQL data
// into a multi dimensional str arr
func GetRowContentsAsMultiStringArr(
	// What do I need?
	// rows *sql.Rows,
	sql string,
	db *sql.DB,
) (*[][]string, error) {
	// Determine length of array via count
	sqlCount := "SELECT COUNT(*) FROM (" + sql + ") AS a"
	var sqlRowCount int
	err := db.QueryRow(sqlCount).Scan(&sqlRowCount)
	if err != nil {
		fmt.Println(sqlRowCount)
		return nil, errors.New("SQL Row Count: " + err.Error())
	}

	fmt.Println("sqlRowCount sqlRowCount sqlRowCount", sqlRowCount)

	// Get array rows
	rows, err := db.Query(sql)
	if err != nil {
		return nil, errors.New("Error trying to fetch rows: " + err.Error())
	}

	// Make multidimensional array to store results
	queryResults := make([][]string, sqlRowCount)

	// Fetch columns from query
	colNames, err := rows.Columns()
	if err != nil {
		log.Fatal(err)
	}
	cols := make([]interface{}, len(colNames))
	colPtrs := make([]interface{}, len(colNames))
	for i := 0; i < len(colNames); i++ {
		colPtrs[i] = &cols[i]
	}

	for i, _ := range queryResults {
		queryResults[i] = make([]string, len(colNames)+1)
	}

	// Iterators
	xIterator := 0
	yIterator := 0

	for rows.Next() {
		err = rows.Scan(colPtrs...)
		if err != nil {
			log.Fatal(err)
		}

		// Always reset y iterator when starting the inner loop
		yIterator = 0
		for _, v := range cols {
			var entry string

			// Make it a byte, asshole because mysql returns fucked up data types like
			// []uint8
			if v != nil {
				if reflect.TypeOf(v).String() == "time.Time" {
					entry = v.(time.Time).String()
				} else {
					x := v.([]byte)

					if nx, ok := strconv.ParseFloat(string(x), 64); ok == nil {
						if math.Mod(nx, 1) == 0 {
							entry = strconv.Itoa(int(nx))
						} else {
							entry = fmt.Sprintf("%f", nx)
						}
					} else if b, ok := strconv.ParseBool(string(x)); ok == nil {
						entry = fmt.Sprintf("%f", b)
					} else if "string" == fmt.Sprintf("%T", string(x)) {
						entry = string(x)
					} else {
						entry = ""
					}
				}
			} else {
				entry = ""
			}

			if entry != "" {
				reComma := regexp.MustCompile(`"`)
				entry = reComma.ReplaceAllString(entry, `""`)
				entry = `"` + entry + `"`
			}

			// Store result in multidimensional array
			queryResults[xIterator][yIterator] = entry
			if yIterator == len(colNames) {
				yIterator = 0

				// Remove last character
				// entry = string_utils.TrimSuffix(entry, ",")
			} else {
				yIterator++
			}
		}
		xIterator++
	}

	// Preprepend column headers
	queryResultWithHeaders := make([][]string, 1)
	for _, v := range colNames {
		queryResultWithHeaders[0] = append(queryResultWithHeaders[0], v)
	}

	queryResults = append(queryResultWithHeaders, queryResults...)

	return &queryResults, nil
}