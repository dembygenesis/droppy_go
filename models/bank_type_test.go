// Code generated by SQLBoiler 4.6.0 (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"bytes"
	"context"
	"reflect"
	"testing"

	"github.com/volatiletech/randomize"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries"
	"github.com/volatiletech/strmangle"
)

var (
	// Relationships sometimes use the reflection helper queries.Equal/queries.Assign
	// so force a package dependency in case they don't.
	_ = queries.Equal
)

func testBankTypes(t *testing.T) {
	t.Parallel()

	query := BankTypes()

	if query.Query == nil {
		t.Error("expected a query, got nothing")
	}
}

func testBankTypesDelete(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := o.Delete(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := BankTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testBankTypesQueryDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := BankTypes().DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := BankTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testBankTypesSliceDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := BankTypeSlice{o}

	if rowsAff, err := slice.DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := BankTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testBankTypesExists(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	e, err := BankTypeExists(ctx, tx, o.ID)
	if err != nil {
		t.Errorf("Unable to check if BankType exists: %s", err)
	}
	if !e {
		t.Errorf("Expected BankTypeExists to return true, but got false.")
	}
}

func testBankTypesFind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	bankTypeFound, err := FindBankType(ctx, tx, o.ID)
	if err != nil {
		t.Error(err)
	}

	if bankTypeFound == nil {
		t.Error("want a record, got nil")
	}
}

func testBankTypesBind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = BankTypes().Bind(ctx, tx, o); err != nil {
		t.Error(err)
	}
}

func testBankTypesOne(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if x, err := BankTypes().One(ctx, tx); err != nil {
		t.Error(err)
	} else if x == nil {
		t.Error("expected to get a non nil record")
	}
}

func testBankTypesAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	bankTypeOne := &BankType{}
	bankTypeTwo := &BankType{}
	if err = randomize.Struct(seed, bankTypeOne, bankTypeDBTypes, false, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}
	if err = randomize.Struct(seed, bankTypeTwo, bankTypeDBTypes, false, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = bankTypeOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = bankTypeTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := BankTypes().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 2 {
		t.Error("want 2 records, got:", len(slice))
	}
}

func testBankTypesCount(t *testing.T) {
	t.Parallel()

	var err error
	seed := randomize.NewSeed()
	bankTypeOne := &BankType{}
	bankTypeTwo := &BankType{}
	if err = randomize.Struct(seed, bankTypeOne, bankTypeDBTypes, false, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}
	if err = randomize.Struct(seed, bankTypeTwo, bankTypeDBTypes, false, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = bankTypeOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = bankTypeTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := BankTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 2 {
		t.Error("want 2 records, got:", count)
	}
}

func bankTypeBeforeInsertHook(ctx context.Context, e boil.ContextExecutor, o *BankType) error {
	*o = BankType{}
	return nil
}

func bankTypeAfterInsertHook(ctx context.Context, e boil.ContextExecutor, o *BankType) error {
	*o = BankType{}
	return nil
}

func bankTypeAfterSelectHook(ctx context.Context, e boil.ContextExecutor, o *BankType) error {
	*o = BankType{}
	return nil
}

func bankTypeBeforeUpdateHook(ctx context.Context, e boil.ContextExecutor, o *BankType) error {
	*o = BankType{}
	return nil
}

func bankTypeAfterUpdateHook(ctx context.Context, e boil.ContextExecutor, o *BankType) error {
	*o = BankType{}
	return nil
}

func bankTypeBeforeDeleteHook(ctx context.Context, e boil.ContextExecutor, o *BankType) error {
	*o = BankType{}
	return nil
}

func bankTypeAfterDeleteHook(ctx context.Context, e boil.ContextExecutor, o *BankType) error {
	*o = BankType{}
	return nil
}

func bankTypeBeforeUpsertHook(ctx context.Context, e boil.ContextExecutor, o *BankType) error {
	*o = BankType{}
	return nil
}

func bankTypeAfterUpsertHook(ctx context.Context, e boil.ContextExecutor, o *BankType) error {
	*o = BankType{}
	return nil
}

func testBankTypesHooks(t *testing.T) {
	t.Parallel()

	var err error

	ctx := context.Background()
	empty := &BankType{}
	o := &BankType{}

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, o, bankTypeDBTypes, false); err != nil {
		t.Errorf("Unable to randomize BankType object: %s", err)
	}

	AddBankTypeHook(boil.BeforeInsertHook, bankTypeBeforeInsertHook)
	if err = o.doBeforeInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeInsertHook function to empty object, but got: %#v", o)
	}
	bankTypeBeforeInsertHooks = []BankTypeHook{}

	AddBankTypeHook(boil.AfterInsertHook, bankTypeAfterInsertHook)
	if err = o.doAfterInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterInsertHook function to empty object, but got: %#v", o)
	}
	bankTypeAfterInsertHooks = []BankTypeHook{}

	AddBankTypeHook(boil.AfterSelectHook, bankTypeAfterSelectHook)
	if err = o.doAfterSelectHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterSelectHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterSelectHook function to empty object, but got: %#v", o)
	}
	bankTypeAfterSelectHooks = []BankTypeHook{}

	AddBankTypeHook(boil.BeforeUpdateHook, bankTypeBeforeUpdateHook)
	if err = o.doBeforeUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpdateHook function to empty object, but got: %#v", o)
	}
	bankTypeBeforeUpdateHooks = []BankTypeHook{}

	AddBankTypeHook(boil.AfterUpdateHook, bankTypeAfterUpdateHook)
	if err = o.doAfterUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpdateHook function to empty object, but got: %#v", o)
	}
	bankTypeAfterUpdateHooks = []BankTypeHook{}

	AddBankTypeHook(boil.BeforeDeleteHook, bankTypeBeforeDeleteHook)
	if err = o.doBeforeDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeDeleteHook function to empty object, but got: %#v", o)
	}
	bankTypeBeforeDeleteHooks = []BankTypeHook{}

	AddBankTypeHook(boil.AfterDeleteHook, bankTypeAfterDeleteHook)
	if err = o.doAfterDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterDeleteHook function to empty object, but got: %#v", o)
	}
	bankTypeAfterDeleteHooks = []BankTypeHook{}

	AddBankTypeHook(boil.BeforeUpsertHook, bankTypeBeforeUpsertHook)
	if err = o.doBeforeUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpsertHook function to empty object, but got: %#v", o)
	}
	bankTypeBeforeUpsertHooks = []BankTypeHook{}

	AddBankTypeHook(boil.AfterUpsertHook, bankTypeAfterUpsertHook)
	if err = o.doAfterUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpsertHook function to empty object, but got: %#v", o)
	}
	bankTypeAfterUpsertHooks = []BankTypeHook{}
}

func testBankTypesInsert(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := BankTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testBankTypesInsertWhitelist(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Whitelist(bankTypeColumnsWithoutDefault...)); err != nil {
		t.Error(err)
	}

	count, err := BankTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testBankTypeToManyTransactions(t *testing.T) {
	var err error
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a BankType
	var b, c Transaction

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = randomize.Struct(seed, &b, transactionDBTypes, false, transactionColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, transactionDBTypes, false, transactionColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}

	b.BankTypeID = a.ID
	c.BankTypeID = a.ID

	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := a.Transactions().All(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	bFound, cFound := false, false
	for _, v := range check {
		if v.BankTypeID == b.BankTypeID {
			bFound = true
		}
		if v.BankTypeID == c.BankTypeID {
			cFound = true
		}
	}

	if !bFound {
		t.Error("expected to find b")
	}
	if !cFound {
		t.Error("expected to find c")
	}

	slice := BankTypeSlice{&a}
	if err = a.L.LoadTransactions(ctx, tx, false, (*[]*BankType)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Transactions); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	a.R.Transactions = nil
	if err = a.L.LoadTransactions(ctx, tx, true, &a, nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Transactions); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	if t.Failed() {
		t.Logf("%#v", check)
	}
}

func testBankTypeToManyUsers(t *testing.T) {
	var err error
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a BankType
	var b, c User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = randomize.Struct(seed, &b, userDBTypes, false, userColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, userDBTypes, false, userColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}

	b.BankTypeID = a.ID
	c.BankTypeID = a.ID

	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := a.Users().All(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	bFound, cFound := false, false
	for _, v := range check {
		if v.BankTypeID == b.BankTypeID {
			bFound = true
		}
		if v.BankTypeID == c.BankTypeID {
			cFound = true
		}
	}

	if !bFound {
		t.Error("expected to find b")
	}
	if !cFound {
		t.Error("expected to find c")
	}

	slice := BankTypeSlice{&a}
	if err = a.L.LoadUsers(ctx, tx, false, (*[]*BankType)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Users); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	a.R.Users = nil
	if err = a.L.LoadUsers(ctx, tx, true, &a, nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Users); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	if t.Failed() {
		t.Logf("%#v", check)
	}
}

func testBankTypeToManyWithdrawals(t *testing.T) {
	var err error
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a BankType
	var b, c Withdrawal

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = randomize.Struct(seed, &b, withdrawalDBTypes, false, withdrawalColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, withdrawalDBTypes, false, withdrawalColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}

	b.BankTypeID = a.ID
	c.BankTypeID = a.ID

	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := a.Withdrawals().All(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	bFound, cFound := false, false
	for _, v := range check {
		if v.BankTypeID == b.BankTypeID {
			bFound = true
		}
		if v.BankTypeID == c.BankTypeID {
			cFound = true
		}
	}

	if !bFound {
		t.Error("expected to find b")
	}
	if !cFound {
		t.Error("expected to find c")
	}

	slice := BankTypeSlice{&a}
	if err = a.L.LoadWithdrawals(ctx, tx, false, (*[]*BankType)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Withdrawals); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	a.R.Withdrawals = nil
	if err = a.L.LoadWithdrawals(ctx, tx, true, &a, nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Withdrawals); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	if t.Failed() {
		t.Logf("%#v", check)
	}
}

func testBankTypeToManyAddOpTransactions(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a BankType
	var b, c, d, e Transaction

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, bankTypeDBTypes, false, strmangle.SetComplement(bankTypePrimaryKeyColumns, bankTypeColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*Transaction{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, transactionDBTypes, false, strmangle.SetComplement(transactionPrimaryKeyColumns, transactionColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	foreignersSplitByInsertion := [][]*Transaction{
		{&b, &c},
		{&d, &e},
	}

	for i, x := range foreignersSplitByInsertion {
		err = a.AddTransactions(ctx, tx, i != 0, x...)
		if err != nil {
			t.Fatal(err)
		}

		first := x[0]
		second := x[1]

		if a.ID != first.BankTypeID {
			t.Error("foreign key was wrong value", a.ID, first.BankTypeID)
		}
		if a.ID != second.BankTypeID {
			t.Error("foreign key was wrong value", a.ID, second.BankTypeID)
		}

		if first.R.BankType != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}
		if second.R.BankType != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}

		if a.R.Transactions[i*2] != first {
			t.Error("relationship struct slice not set to correct value")
		}
		if a.R.Transactions[i*2+1] != second {
			t.Error("relationship struct slice not set to correct value")
		}

		count, err := a.Transactions().Count(ctx, tx)
		if err != nil {
			t.Fatal(err)
		}
		if want := int64((i + 1) * 2); count != want {
			t.Error("want", want, "got", count)
		}
	}
}
func testBankTypeToManyAddOpUsers(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a BankType
	var b, c, d, e User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, bankTypeDBTypes, false, strmangle.SetComplement(bankTypePrimaryKeyColumns, bankTypeColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*User{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	foreignersSplitByInsertion := [][]*User{
		{&b, &c},
		{&d, &e},
	}

	for i, x := range foreignersSplitByInsertion {
		err = a.AddUsers(ctx, tx, i != 0, x...)
		if err != nil {
			t.Fatal(err)
		}

		first := x[0]
		second := x[1]

		if a.ID != first.BankTypeID {
			t.Error("foreign key was wrong value", a.ID, first.BankTypeID)
		}
		if a.ID != second.BankTypeID {
			t.Error("foreign key was wrong value", a.ID, second.BankTypeID)
		}

		if first.R.BankType != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}
		if second.R.BankType != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}

		if a.R.Users[i*2] != first {
			t.Error("relationship struct slice not set to correct value")
		}
		if a.R.Users[i*2+1] != second {
			t.Error("relationship struct slice not set to correct value")
		}

		count, err := a.Users().Count(ctx, tx)
		if err != nil {
			t.Fatal(err)
		}
		if want := int64((i + 1) * 2); count != want {
			t.Error("want", want, "got", count)
		}
	}
}
func testBankTypeToManyAddOpWithdrawals(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a BankType
	var b, c, d, e Withdrawal

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, bankTypeDBTypes, false, strmangle.SetComplement(bankTypePrimaryKeyColumns, bankTypeColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*Withdrawal{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, withdrawalDBTypes, false, strmangle.SetComplement(withdrawalPrimaryKeyColumns, withdrawalColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	foreignersSplitByInsertion := [][]*Withdrawal{
		{&b, &c},
		{&d, &e},
	}

	for i, x := range foreignersSplitByInsertion {
		err = a.AddWithdrawals(ctx, tx, i != 0, x...)
		if err != nil {
			t.Fatal(err)
		}

		first := x[0]
		second := x[1]

		if a.ID != first.BankTypeID {
			t.Error("foreign key was wrong value", a.ID, first.BankTypeID)
		}
		if a.ID != second.BankTypeID {
			t.Error("foreign key was wrong value", a.ID, second.BankTypeID)
		}

		if first.R.BankType != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}
		if second.R.BankType != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}

		if a.R.Withdrawals[i*2] != first {
			t.Error("relationship struct slice not set to correct value")
		}
		if a.R.Withdrawals[i*2+1] != second {
			t.Error("relationship struct slice not set to correct value")
		}

		count, err := a.Withdrawals().Count(ctx, tx)
		if err != nil {
			t.Fatal(err)
		}
		if want := int64((i + 1) * 2); count != want {
			t.Error("want", want, "got", count)
		}
	}
}
func testBankTypeToOneUserUsingModifiedByUser(t *testing.T) {
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var local BankType
	var foreign User

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, userDBTypes, false, userColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize User struct: %s", err)
	}

	if err := foreign.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	queries.Assign(&local.ModifiedBy, foreign.ID)
	if err := local.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := local.ModifiedByUser().One(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	if !queries.Equal(check.ID, foreign.ID) {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := BankTypeSlice{&local}
	if err = local.L.LoadModifiedByUser(ctx, tx, false, (*[]*BankType)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if local.R.ModifiedByUser == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.ModifiedByUser = nil
	if err = local.L.LoadModifiedByUser(ctx, tx, true, &local, nil); err != nil {
		t.Fatal(err)
	}
	if local.R.ModifiedByUser == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testBankTypeToOneSetOpUserUsingModifiedByUser(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a BankType
	var b, c User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, bankTypeDBTypes, false, strmangle.SetComplement(bankTypePrimaryKeyColumns, bankTypeColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*User{&b, &c} {
		err = a.SetModifiedByUser(ctx, tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.ModifiedByUser != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.ModifiedByBankTypes[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if !queries.Equal(a.ModifiedBy, x.ID) {
			t.Error("foreign key was wrong value", a.ModifiedBy)
		}

		zero := reflect.Zero(reflect.TypeOf(a.ModifiedBy))
		reflect.Indirect(reflect.ValueOf(&a.ModifiedBy)).Set(zero)

		if err = a.Reload(ctx, tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if !queries.Equal(a.ModifiedBy, x.ID) {
			t.Error("foreign key was wrong value", a.ModifiedBy, x.ID)
		}
	}
}

func testBankTypeToOneRemoveOpUserUsingModifiedByUser(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a BankType
	var b User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, bankTypeDBTypes, false, strmangle.SetComplement(bankTypePrimaryKeyColumns, bankTypeColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err = a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = a.SetModifiedByUser(ctx, tx, true, &b); err != nil {
		t.Fatal(err)
	}

	if err = a.RemoveModifiedByUser(ctx, tx, &b); err != nil {
		t.Error("failed to remove relationship")
	}

	count, err := a.ModifiedByUser().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 0 {
		t.Error("want no relationships remaining")
	}

	if a.R.ModifiedByUser != nil {
		t.Error("R struct entry should be nil")
	}

	if !queries.IsValuerNil(a.ModifiedBy) {
		t.Error("foreign key value should be nil")
	}

	if len(b.R.ModifiedByBankTypes) != 0 {
		t.Error("failed to remove a from b's relationships")
	}
}

func testBankTypesReload(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = o.Reload(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testBankTypesReloadAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := BankTypeSlice{o}

	if err = slice.ReloadAll(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testBankTypesSelect(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := BankTypes().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 1 {
		t.Error("want one record, got:", len(slice))
	}
}

var (
	bankTypeDBTypes = map[string]string{`ID`: `int`, `Name`: `varchar`, `DateAdded`: `timestamp`, `DateModified`: `timestamp`, `ModifiedBy`: `int`, `Voided`: `tinyint`}
	_               = bytes.MinRead
)

func testBankTypesUpdate(t *testing.T) {
	t.Parallel()

	if 0 == len(bankTypePrimaryKeyColumns) {
		t.Skip("Skipping table with no primary key columns")
	}
	if len(bankTypeAllColumns) == len(bankTypePrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := BankTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypePrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	if rowsAff, err := o.Update(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only affect one row but affected", rowsAff)
	}
}

func testBankTypesSliceUpdateAll(t *testing.T) {
	t.Parallel()

	if len(bankTypeAllColumns) == len(bankTypePrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &BankType{}
	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := BankTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, bankTypeDBTypes, true, bankTypePrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	// Remove Primary keys and unique columns from what we plan to update
	var fields []string
	if strmangle.StringSliceMatch(bankTypeAllColumns, bankTypePrimaryKeyColumns) {
		fields = bankTypeAllColumns
	} else {
		fields = strmangle.SetComplement(
			bankTypeAllColumns,
			bankTypePrimaryKeyColumns,
		)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	typ := reflect.TypeOf(o).Elem()
	n := typ.NumField()

	updateMap := M{}
	for _, col := range fields {
		for i := 0; i < n; i++ {
			f := typ.Field(i)
			if f.Tag.Get("boil") == col {
				updateMap[col] = value.Field(i).Interface()
			}
		}
	}

	slice := BankTypeSlice{o}
	if rowsAff, err := slice.UpdateAll(ctx, tx, updateMap); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("wanted one record updated but got", rowsAff)
	}
}

func testBankTypesUpsert(t *testing.T) {
	t.Parallel()

	if len(bankTypeAllColumns) == len(bankTypePrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}
	if len(mySQLBankTypeUniqueColumns) == 0 {
		t.Skip("Skipping table with no unique columns to conflict on")
	}

	seed := randomize.NewSeed()
	var err error
	// Attempt the INSERT side of an UPSERT
	o := BankType{}
	if err = randomize.Struct(seed, &o, bankTypeDBTypes, false); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Upsert(ctx, tx, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert BankType: %s", err)
	}

	count, err := BankTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}

	// Attempt the UPDATE side of an UPSERT
	if err = randomize.Struct(seed, &o, bankTypeDBTypes, false, bankTypePrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize BankType struct: %s", err)
	}

	if err = o.Upsert(ctx, tx, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert BankType: %s", err)
	}

	count, err = BankTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}
}
