// Code generated by SQLBoiler 4.6.0 (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"bytes"
	"context"
	"reflect"
	"testing"

	"github.com/volatiletech/randomize"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries"
	"github.com/volatiletech/strmangle"
)

var (
	// Relationships sometimes use the reflection helper queries.Equal/queries.Assign
	// so force a package dependency in case they don't.
	_ = queries.Equal
)

func testUserTypes(t *testing.T) {
	t.Parallel()

	query := UserTypes()

	if query.Query == nil {
		t.Error("expected a query, got nothing")
	}
}

func testUserTypesDelete(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := o.Delete(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := UserTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testUserTypesQueryDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := UserTypes().DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := UserTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testUserTypesSliceDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := UserTypeSlice{o}

	if rowsAff, err := slice.DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := UserTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testUserTypesExists(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	e, err := UserTypeExists(ctx, tx, o.ID)
	if err != nil {
		t.Errorf("Unable to check if UserType exists: %s", err)
	}
	if !e {
		t.Errorf("Expected UserTypeExists to return true, but got false.")
	}
}

func testUserTypesFind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	userTypeFound, err := FindUserType(ctx, tx, o.ID)
	if err != nil {
		t.Error(err)
	}

	if userTypeFound == nil {
		t.Error("want a record, got nil")
	}
}

func testUserTypesBind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = UserTypes().Bind(ctx, tx, o); err != nil {
		t.Error(err)
	}
}

func testUserTypesOne(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if x, err := UserTypes().One(ctx, tx); err != nil {
		t.Error(err)
	} else if x == nil {
		t.Error("expected to get a non nil record")
	}
}

func testUserTypesAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	userTypeOne := &UserType{}
	userTypeTwo := &UserType{}
	if err = randomize.Struct(seed, userTypeOne, userTypeDBTypes, false, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}
	if err = randomize.Struct(seed, userTypeTwo, userTypeDBTypes, false, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = userTypeOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = userTypeTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := UserTypes().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 2 {
		t.Error("want 2 records, got:", len(slice))
	}
}

func testUserTypesCount(t *testing.T) {
	t.Parallel()

	var err error
	seed := randomize.NewSeed()
	userTypeOne := &UserType{}
	userTypeTwo := &UserType{}
	if err = randomize.Struct(seed, userTypeOne, userTypeDBTypes, false, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}
	if err = randomize.Struct(seed, userTypeTwo, userTypeDBTypes, false, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = userTypeOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = userTypeTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := UserTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 2 {
		t.Error("want 2 records, got:", count)
	}
}

func userTypeBeforeInsertHook(ctx context.Context, e boil.ContextExecutor, o *UserType) error {
	*o = UserType{}
	return nil
}

func userTypeAfterInsertHook(ctx context.Context, e boil.ContextExecutor, o *UserType) error {
	*o = UserType{}
	return nil
}

func userTypeAfterSelectHook(ctx context.Context, e boil.ContextExecutor, o *UserType) error {
	*o = UserType{}
	return nil
}

func userTypeBeforeUpdateHook(ctx context.Context, e boil.ContextExecutor, o *UserType) error {
	*o = UserType{}
	return nil
}

func userTypeAfterUpdateHook(ctx context.Context, e boil.ContextExecutor, o *UserType) error {
	*o = UserType{}
	return nil
}

func userTypeBeforeDeleteHook(ctx context.Context, e boil.ContextExecutor, o *UserType) error {
	*o = UserType{}
	return nil
}

func userTypeAfterDeleteHook(ctx context.Context, e boil.ContextExecutor, o *UserType) error {
	*o = UserType{}
	return nil
}

func userTypeBeforeUpsertHook(ctx context.Context, e boil.ContextExecutor, o *UserType) error {
	*o = UserType{}
	return nil
}

func userTypeAfterUpsertHook(ctx context.Context, e boil.ContextExecutor, o *UserType) error {
	*o = UserType{}
	return nil
}

func testUserTypesHooks(t *testing.T) {
	t.Parallel()

	var err error

	ctx := context.Background()
	empty := &UserType{}
	o := &UserType{}

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, o, userTypeDBTypes, false); err != nil {
		t.Errorf("Unable to randomize UserType object: %s", err)
	}

	AddUserTypeHook(boil.BeforeInsertHook, userTypeBeforeInsertHook)
	if err = o.doBeforeInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeInsertHook function to empty object, but got: %#v", o)
	}
	userTypeBeforeInsertHooks = []UserTypeHook{}

	AddUserTypeHook(boil.AfterInsertHook, userTypeAfterInsertHook)
	if err = o.doAfterInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterInsertHook function to empty object, but got: %#v", o)
	}
	userTypeAfterInsertHooks = []UserTypeHook{}

	AddUserTypeHook(boil.AfterSelectHook, userTypeAfterSelectHook)
	if err = o.doAfterSelectHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterSelectHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterSelectHook function to empty object, but got: %#v", o)
	}
	userTypeAfterSelectHooks = []UserTypeHook{}

	AddUserTypeHook(boil.BeforeUpdateHook, userTypeBeforeUpdateHook)
	if err = o.doBeforeUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpdateHook function to empty object, but got: %#v", o)
	}
	userTypeBeforeUpdateHooks = []UserTypeHook{}

	AddUserTypeHook(boil.AfterUpdateHook, userTypeAfterUpdateHook)
	if err = o.doAfterUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpdateHook function to empty object, but got: %#v", o)
	}
	userTypeAfterUpdateHooks = []UserTypeHook{}

	AddUserTypeHook(boil.BeforeDeleteHook, userTypeBeforeDeleteHook)
	if err = o.doBeforeDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeDeleteHook function to empty object, but got: %#v", o)
	}
	userTypeBeforeDeleteHooks = []UserTypeHook{}

	AddUserTypeHook(boil.AfterDeleteHook, userTypeAfterDeleteHook)
	if err = o.doAfterDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterDeleteHook function to empty object, but got: %#v", o)
	}
	userTypeAfterDeleteHooks = []UserTypeHook{}

	AddUserTypeHook(boil.BeforeUpsertHook, userTypeBeforeUpsertHook)
	if err = o.doBeforeUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpsertHook function to empty object, but got: %#v", o)
	}
	userTypeBeforeUpsertHooks = []UserTypeHook{}

	AddUserTypeHook(boil.AfterUpsertHook, userTypeAfterUpsertHook)
	if err = o.doAfterUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpsertHook function to empty object, but got: %#v", o)
	}
	userTypeAfterUpsertHooks = []UserTypeHook{}
}

func testUserTypesInsert(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := UserTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testUserTypesInsertWhitelist(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Whitelist(userTypeColumnsWithoutDefault...)); err != nil {
		t.Error(err)
	}

	count, err := UserTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testUserTypeToManyUsers(t *testing.T) {
	var err error
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a UserType
	var b, c User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = randomize.Struct(seed, &b, userDBTypes, false, userColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, userDBTypes, false, userColumnsWithDefault...); err != nil {
		t.Fatal(err)
	}

	b.UserTypeID = a.ID
	c.UserTypeID = a.ID

	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := a.Users().All(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	bFound, cFound := false, false
	for _, v := range check {
		if v.UserTypeID == b.UserTypeID {
			bFound = true
		}
		if v.UserTypeID == c.UserTypeID {
			cFound = true
		}
	}

	if !bFound {
		t.Error("expected to find b")
	}
	if !cFound {
		t.Error("expected to find c")
	}

	slice := UserTypeSlice{&a}
	if err = a.L.LoadUsers(ctx, tx, false, (*[]*UserType)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Users); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	a.R.Users = nil
	if err = a.L.LoadUsers(ctx, tx, true, &a, nil); err != nil {
		t.Fatal(err)
	}
	if got := len(a.R.Users); got != 2 {
		t.Error("number of eager loaded records wrong, got:", got)
	}

	if t.Failed() {
		t.Logf("%#v", check)
	}
}

func testUserTypeToManyAddOpUsers(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a UserType
	var b, c, d, e User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, userTypeDBTypes, false, strmangle.SetComplement(userTypePrimaryKeyColumns, userTypeColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	foreigners := []*User{&b, &c, &d, &e}
	for _, x := range foreigners {
		if err = randomize.Struct(seed, x, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
			t.Fatal(err)
		}
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = c.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	foreignersSplitByInsertion := [][]*User{
		{&b, &c},
		{&d, &e},
	}

	for i, x := range foreignersSplitByInsertion {
		err = a.AddUsers(ctx, tx, i != 0, x...)
		if err != nil {
			t.Fatal(err)
		}

		first := x[0]
		second := x[1]

		if a.ID != first.UserTypeID {
			t.Error("foreign key was wrong value", a.ID, first.UserTypeID)
		}
		if a.ID != second.UserTypeID {
			t.Error("foreign key was wrong value", a.ID, second.UserTypeID)
		}

		if first.R.UserType != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}
		if second.R.UserType != &a {
			t.Error("relationship was not added properly to the foreign slice")
		}

		if a.R.Users[i*2] != first {
			t.Error("relationship struct slice not set to correct value")
		}
		if a.R.Users[i*2+1] != second {
			t.Error("relationship struct slice not set to correct value")
		}

		count, err := a.Users().Count(ctx, tx)
		if err != nil {
			t.Fatal(err)
		}
		if want := int64((i + 1) * 2); count != want {
			t.Error("want", want, "got", count)
		}
	}
}

func testUserTypesReload(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = o.Reload(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testUserTypesReloadAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := UserTypeSlice{o}

	if err = slice.ReloadAll(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testUserTypesSelect(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := UserTypes().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 1 {
		t.Error("want one record, got:", len(slice))
	}
}

var (
	userTypeDBTypes = map[string]string{`ID`: `int`, `Name`: `varchar`}
	_               = bytes.MinRead
)

func testUserTypesUpdate(t *testing.T) {
	t.Parallel()

	if 0 == len(userTypePrimaryKeyColumns) {
		t.Skip("Skipping table with no primary key columns")
	}
	if len(userTypeAllColumns) == len(userTypePrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := UserTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypePrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	if rowsAff, err := o.Update(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only affect one row but affected", rowsAff)
	}
}

func testUserTypesSliceUpdateAll(t *testing.T) {
	t.Parallel()

	if len(userTypeAllColumns) == len(userTypePrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &UserType{}
	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypeColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := UserTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, userTypeDBTypes, true, userTypePrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	// Remove Primary keys and unique columns from what we plan to update
	var fields []string
	if strmangle.StringSliceMatch(userTypeAllColumns, userTypePrimaryKeyColumns) {
		fields = userTypeAllColumns
	} else {
		fields = strmangle.SetComplement(
			userTypeAllColumns,
			userTypePrimaryKeyColumns,
		)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	typ := reflect.TypeOf(o).Elem()
	n := typ.NumField()

	updateMap := M{}
	for _, col := range fields {
		for i := 0; i < n; i++ {
			f := typ.Field(i)
			if f.Tag.Get("boil") == col {
				updateMap[col] = value.Field(i).Interface()
			}
		}
	}

	slice := UserTypeSlice{o}
	if rowsAff, err := slice.UpdateAll(ctx, tx, updateMap); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("wanted one record updated but got", rowsAff)
	}
}

func testUserTypesUpsert(t *testing.T) {
	t.Parallel()

	if len(userTypeAllColumns) == len(userTypePrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}
	if len(mySQLUserTypeUniqueColumns) == 0 {
		t.Skip("Skipping table with no unique columns to conflict on")
	}

	seed := randomize.NewSeed()
	var err error
	// Attempt the INSERT side of an UPSERT
	o := UserType{}
	if err = randomize.Struct(seed, &o, userTypeDBTypes, false); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Upsert(ctx, tx, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert UserType: %s", err)
	}

	count, err := UserTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}

	// Attempt the UPDATE side of an UPSERT
	if err = randomize.Struct(seed, &o, userTypeDBTypes, false, userTypePrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize UserType struct: %s", err)
	}

	if err = o.Upsert(ctx, tx, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert UserType: %s", err)
	}

	count, err = UserTypes().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}
}
