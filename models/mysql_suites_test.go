// Code generated by SQLBoiler 4.6.0 (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import "testing"

func TestUpsert(t *testing.T) {
	t.Run("BankTypes", testBankTypesUpsert)

	t.Run("CoinTransactions", testCoinTransactionsUpsert)

	t.Run("Deliveries", testDeliveriesUpsert)

	t.Run("DeliveryCouriers", testDeliveryCouriersUpsert)

	t.Run("DeliveryCourierRates", testDeliveryCourierRatesUpsert)

	t.Run("DeliveryDetails", testDeliveryDetailsUpsert)

	t.Run("DeliveryOptions", testDeliveryOptionsUpsert)

	t.Run("DeliveryStatuses", testDeliveryStatusesUpsert)

	t.Run("DeliveryTrackings", testDeliveryTrackingsUpsert)

	t.Run("DroppyTransactions", testDroppyTransactionsUpsert)

	t.Run("DroppyUserTotals", testDroppyUserTotalsUpsert)

	t.Run("Inventories", testInventoriesUpsert)

	t.Run("Orders", testOrdersUpsert)

	t.Run("OrderDetails", testOrderDetailsUpsert)

	t.Run("OrderStatuses", testOrderStatusesUpsert)

	t.Run("Products", testProductsUpsert)

	t.Run("ProductTypes", testProductTypesUpsert)

	t.Run("Regions", testRegionsUpsert)

	t.Run("Sysparams", testSysparamsUpsert)

	t.Run("Tests", testTestsUpsert)

	t.Run("Transactions", testTransactionsUpsert)

	t.Run("Users", testUsersUpsert)

	t.Run("UserTotals", testUserTotalsUpsert)

	t.Run("UserTypes", testUserTypesUpsert)

	t.Run("Withdrawals", testWithdrawalsUpsert)

	t.Run("WithdrawalStatuses", testWithdrawalStatusesUpsert)
}
