// Code generated by SQLBoiler 4.6.0 (https://github.com/volatiletech/sqlboiler). DO NOT EDIT.
// This file is meant to be re-generated in place and/or deleted at any time.

package models

import (
	"bytes"
	"context"
	"reflect"
	"testing"

	"github.com/volatiletech/randomize"
	"github.com/volatiletech/sqlboiler/v4/boil"
	"github.com/volatiletech/sqlboiler/v4/queries"
	"github.com/volatiletech/strmangle"
)

var (
	// Relationships sometimes use the reflection helper queries.Equal/queries.Assign
	// so force a package dependency in case they don't.
	_ = queries.Equal
)

func testInventories(t *testing.T) {
	t.Parallel()

	query := Inventories()

	if query.Query == nil {
		t.Error("expected a query, got nothing")
	}
}

func testInventoriesDelete(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := o.Delete(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := Inventories().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testInventoriesQueryDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if rowsAff, err := Inventories().DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := Inventories().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testInventoriesSliceDeleteAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := InventorySlice{o}

	if rowsAff, err := slice.DeleteAll(ctx, tx); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only have deleted one row, but affected:", rowsAff)
	}

	count, err := Inventories().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Error("want zero records, got:", count)
	}
}

func testInventoriesExists(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	e, err := InventoryExists(ctx, tx, o.ID)
	if err != nil {
		t.Errorf("Unable to check if Inventory exists: %s", err)
	}
	if !e {
		t.Errorf("Expected InventoryExists to return true, but got false.")
	}
}

func testInventoriesFind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	inventoryFound, err := FindInventory(ctx, tx, o.ID)
	if err != nil {
		t.Error(err)
	}

	if inventoryFound == nil {
		t.Error("want a record, got nil")
	}
}

func testInventoriesBind(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = Inventories().Bind(ctx, tx, o); err != nil {
		t.Error(err)
	}
}

func testInventoriesOne(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if x, err := Inventories().One(ctx, tx); err != nil {
		t.Error(err)
	} else if x == nil {
		t.Error("expected to get a non nil record")
	}
}

func testInventoriesAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	inventoryOne := &Inventory{}
	inventoryTwo := &Inventory{}
	if err = randomize.Struct(seed, inventoryOne, inventoryDBTypes, false, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}
	if err = randomize.Struct(seed, inventoryTwo, inventoryDBTypes, false, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = inventoryOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = inventoryTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := Inventories().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 2 {
		t.Error("want 2 records, got:", len(slice))
	}
}

func testInventoriesCount(t *testing.T) {
	t.Parallel()

	var err error
	seed := randomize.NewSeed()
	inventoryOne := &Inventory{}
	inventoryTwo := &Inventory{}
	if err = randomize.Struct(seed, inventoryOne, inventoryDBTypes, false, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}
	if err = randomize.Struct(seed, inventoryTwo, inventoryDBTypes, false, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = inventoryOne.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}
	if err = inventoryTwo.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Inventories().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 2 {
		t.Error("want 2 records, got:", count)
	}
}

func inventoryBeforeInsertHook(ctx context.Context, e boil.ContextExecutor, o *Inventory) error {
	*o = Inventory{}
	return nil
}

func inventoryAfterInsertHook(ctx context.Context, e boil.ContextExecutor, o *Inventory) error {
	*o = Inventory{}
	return nil
}

func inventoryAfterSelectHook(ctx context.Context, e boil.ContextExecutor, o *Inventory) error {
	*o = Inventory{}
	return nil
}

func inventoryBeforeUpdateHook(ctx context.Context, e boil.ContextExecutor, o *Inventory) error {
	*o = Inventory{}
	return nil
}

func inventoryAfterUpdateHook(ctx context.Context, e boil.ContextExecutor, o *Inventory) error {
	*o = Inventory{}
	return nil
}

func inventoryBeforeDeleteHook(ctx context.Context, e boil.ContextExecutor, o *Inventory) error {
	*o = Inventory{}
	return nil
}

func inventoryAfterDeleteHook(ctx context.Context, e boil.ContextExecutor, o *Inventory) error {
	*o = Inventory{}
	return nil
}

func inventoryBeforeUpsertHook(ctx context.Context, e boil.ContextExecutor, o *Inventory) error {
	*o = Inventory{}
	return nil
}

func inventoryAfterUpsertHook(ctx context.Context, e boil.ContextExecutor, o *Inventory) error {
	*o = Inventory{}
	return nil
}

func testInventoriesHooks(t *testing.T) {
	t.Parallel()

	var err error

	ctx := context.Background()
	empty := &Inventory{}
	o := &Inventory{}

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, o, inventoryDBTypes, false); err != nil {
		t.Errorf("Unable to randomize Inventory object: %s", err)
	}

	AddInventoryHook(boil.BeforeInsertHook, inventoryBeforeInsertHook)
	if err = o.doBeforeInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeInsertHook function to empty object, but got: %#v", o)
	}
	inventoryBeforeInsertHooks = []InventoryHook{}

	AddInventoryHook(boil.AfterInsertHook, inventoryAfterInsertHook)
	if err = o.doAfterInsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterInsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterInsertHook function to empty object, but got: %#v", o)
	}
	inventoryAfterInsertHooks = []InventoryHook{}

	AddInventoryHook(boil.AfterSelectHook, inventoryAfterSelectHook)
	if err = o.doAfterSelectHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterSelectHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterSelectHook function to empty object, but got: %#v", o)
	}
	inventoryAfterSelectHooks = []InventoryHook{}

	AddInventoryHook(boil.BeforeUpdateHook, inventoryBeforeUpdateHook)
	if err = o.doBeforeUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpdateHook function to empty object, but got: %#v", o)
	}
	inventoryBeforeUpdateHooks = []InventoryHook{}

	AddInventoryHook(boil.AfterUpdateHook, inventoryAfterUpdateHook)
	if err = o.doAfterUpdateHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpdateHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpdateHook function to empty object, but got: %#v", o)
	}
	inventoryAfterUpdateHooks = []InventoryHook{}

	AddInventoryHook(boil.BeforeDeleteHook, inventoryBeforeDeleteHook)
	if err = o.doBeforeDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeDeleteHook function to empty object, but got: %#v", o)
	}
	inventoryBeforeDeleteHooks = []InventoryHook{}

	AddInventoryHook(boil.AfterDeleteHook, inventoryAfterDeleteHook)
	if err = o.doAfterDeleteHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterDeleteHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterDeleteHook function to empty object, but got: %#v", o)
	}
	inventoryAfterDeleteHooks = []InventoryHook{}

	AddInventoryHook(boil.BeforeUpsertHook, inventoryBeforeUpsertHook)
	if err = o.doBeforeUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doBeforeUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected BeforeUpsertHook function to empty object, but got: %#v", o)
	}
	inventoryBeforeUpsertHooks = []InventoryHook{}

	AddInventoryHook(boil.AfterUpsertHook, inventoryAfterUpsertHook)
	if err = o.doAfterUpsertHooks(ctx, nil); err != nil {
		t.Errorf("Unable to execute doAfterUpsertHooks: %s", err)
	}
	if !reflect.DeepEqual(o, empty) {
		t.Errorf("Expected AfterUpsertHook function to empty object, but got: %#v", o)
	}
	inventoryAfterUpsertHooks = []InventoryHook{}
}

func testInventoriesInsert(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Inventories().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testInventoriesInsertWhitelist(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Whitelist(inventoryColumnsWithoutDefault...)); err != nil {
		t.Error(err)
	}

	count, err := Inventories().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}
}

func testInventoryToOneProductUsingProduct(t *testing.T) {
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var local Inventory
	var foreign Product

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, inventoryDBTypes, false, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, productDBTypes, false, productColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Product struct: %s", err)
	}

	if err := foreign.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	local.ProductID = foreign.ID
	if err := local.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := local.Product().One(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	if check.ID != foreign.ID {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := InventorySlice{&local}
	if err = local.L.LoadProduct(ctx, tx, false, (*[]*Inventory)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if local.R.Product == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.Product = nil
	if err = local.L.LoadProduct(ctx, tx, true, &local, nil); err != nil {
		t.Fatal(err)
	}
	if local.R.Product == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testInventoryToOneUserUsingCreatedByUser(t *testing.T) {
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var local Inventory
	var foreign User

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, inventoryDBTypes, false, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, userDBTypes, false, userColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize User struct: %s", err)
	}

	if err := foreign.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	local.CreatedBy = foreign.ID
	if err := local.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := local.CreatedByUser().One(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	if check.ID != foreign.ID {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := InventorySlice{&local}
	if err = local.L.LoadCreatedByUser(ctx, tx, false, (*[]*Inventory)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if local.R.CreatedByUser == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.CreatedByUser = nil
	if err = local.L.LoadCreatedByUser(ctx, tx, true, &local, nil); err != nil {
		t.Fatal(err)
	}
	if local.R.CreatedByUser == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testInventoryToOneUserUsingUpdatedByUser(t *testing.T) {
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var local Inventory
	var foreign User

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, userDBTypes, false, userColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize User struct: %s", err)
	}

	if err := foreign.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	queries.Assign(&local.UpdatedBy, foreign.ID)
	if err := local.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := local.UpdatedByUser().One(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	if !queries.Equal(check.ID, foreign.ID) {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := InventorySlice{&local}
	if err = local.L.LoadUpdatedByUser(ctx, tx, false, (*[]*Inventory)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if local.R.UpdatedByUser == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.UpdatedByUser = nil
	if err = local.L.LoadUpdatedByUser(ctx, tx, true, &local, nil); err != nil {
		t.Fatal(err)
	}
	if local.R.UpdatedByUser == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testInventoryToOneRegionUsingRegion(t *testing.T) {
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var local Inventory
	var foreign Region

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, inventoryDBTypes, false, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, regionDBTypes, false, regionColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Region struct: %s", err)
	}

	if err := foreign.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	local.RegionID = foreign.ID
	if err := local.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := local.Region().One(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	if check.ID != foreign.ID {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := InventorySlice{&local}
	if err = local.L.LoadRegion(ctx, tx, false, (*[]*Inventory)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if local.R.Region == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.Region = nil
	if err = local.L.LoadRegion(ctx, tx, true, &local, nil); err != nil {
		t.Fatal(err)
	}
	if local.R.Region == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testInventoryToOneUserUsingSeller(t *testing.T) {
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var local Inventory
	var foreign User

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, inventoryDBTypes, false, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, userDBTypes, false, userColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize User struct: %s", err)
	}

	if err := foreign.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	local.SellerID = foreign.ID
	if err := local.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := local.Seller().One(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	if check.ID != foreign.ID {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := InventorySlice{&local}
	if err = local.L.LoadSeller(ctx, tx, false, (*[]*Inventory)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if local.R.Seller == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.Seller = nil
	if err = local.L.LoadSeller(ctx, tx, true, &local, nil); err != nil {
		t.Fatal(err)
	}
	if local.R.Seller == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testInventoryToOneUserUsingDropshipper(t *testing.T) {
	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var local Inventory
	var foreign User

	seed := randomize.NewSeed()
	if err := randomize.Struct(seed, &local, inventoryDBTypes, false, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}
	if err := randomize.Struct(seed, &foreign, userDBTypes, false, userColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize User struct: %s", err)
	}

	if err := foreign.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	local.DropshipperID = foreign.ID
	if err := local.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	check, err := local.Dropshipper().One(ctx, tx)
	if err != nil {
		t.Fatal(err)
	}

	if check.ID != foreign.ID {
		t.Errorf("want: %v, got %v", foreign.ID, check.ID)
	}

	slice := InventorySlice{&local}
	if err = local.L.LoadDropshipper(ctx, tx, false, (*[]*Inventory)(&slice), nil); err != nil {
		t.Fatal(err)
	}
	if local.R.Dropshipper == nil {
		t.Error("struct should have been eager loaded")
	}

	local.R.Dropshipper = nil
	if err = local.L.LoadDropshipper(ctx, tx, true, &local, nil); err != nil {
		t.Fatal(err)
	}
	if local.R.Dropshipper == nil {
		t.Error("struct should have been eager loaded")
	}
}

func testInventoryToOneSetOpProductUsingProduct(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Inventory
	var b, c Product

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, inventoryDBTypes, false, strmangle.SetComplement(inventoryPrimaryKeyColumns, inventoryColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, productDBTypes, false, strmangle.SetComplement(productPrimaryKeyColumns, productColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, productDBTypes, false, strmangle.SetComplement(productPrimaryKeyColumns, productColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*Product{&b, &c} {
		err = a.SetProduct(ctx, tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.Product != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.Inventories[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if a.ProductID != x.ID {
			t.Error("foreign key was wrong value", a.ProductID)
		}

		zero := reflect.Zero(reflect.TypeOf(a.ProductID))
		reflect.Indirect(reflect.ValueOf(&a.ProductID)).Set(zero)

		if err = a.Reload(ctx, tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if a.ProductID != x.ID {
			t.Error("foreign key was wrong value", a.ProductID, x.ID)
		}
	}
}
func testInventoryToOneSetOpUserUsingCreatedByUser(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Inventory
	var b, c User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, inventoryDBTypes, false, strmangle.SetComplement(inventoryPrimaryKeyColumns, inventoryColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*User{&b, &c} {
		err = a.SetCreatedByUser(ctx, tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.CreatedByUser != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.CreatedByInventories[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if a.CreatedBy != x.ID {
			t.Error("foreign key was wrong value", a.CreatedBy)
		}

		zero := reflect.Zero(reflect.TypeOf(a.CreatedBy))
		reflect.Indirect(reflect.ValueOf(&a.CreatedBy)).Set(zero)

		if err = a.Reload(ctx, tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if a.CreatedBy != x.ID {
			t.Error("foreign key was wrong value", a.CreatedBy, x.ID)
		}
	}
}
func testInventoryToOneSetOpUserUsingUpdatedByUser(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Inventory
	var b, c User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, inventoryDBTypes, false, strmangle.SetComplement(inventoryPrimaryKeyColumns, inventoryColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*User{&b, &c} {
		err = a.SetUpdatedByUser(ctx, tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.UpdatedByUser != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.UpdatedByInventories[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if !queries.Equal(a.UpdatedBy, x.ID) {
			t.Error("foreign key was wrong value", a.UpdatedBy)
		}

		zero := reflect.Zero(reflect.TypeOf(a.UpdatedBy))
		reflect.Indirect(reflect.ValueOf(&a.UpdatedBy)).Set(zero)

		if err = a.Reload(ctx, tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if !queries.Equal(a.UpdatedBy, x.ID) {
			t.Error("foreign key was wrong value", a.UpdatedBy, x.ID)
		}
	}
}

func testInventoryToOneRemoveOpUserUsingUpdatedByUser(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Inventory
	var b User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, inventoryDBTypes, false, strmangle.SetComplement(inventoryPrimaryKeyColumns, inventoryColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err = a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	if err = a.SetUpdatedByUser(ctx, tx, true, &b); err != nil {
		t.Fatal(err)
	}

	if err = a.RemoveUpdatedByUser(ctx, tx, &b); err != nil {
		t.Error("failed to remove relationship")
	}

	count, err := a.UpdatedByUser().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 0 {
		t.Error("want no relationships remaining")
	}

	if a.R.UpdatedByUser != nil {
		t.Error("R struct entry should be nil")
	}

	if !queries.IsValuerNil(a.UpdatedBy) {
		t.Error("foreign key value should be nil")
	}

	if len(b.R.UpdatedByInventories) != 0 {
		t.Error("failed to remove a from b's relationships")
	}
}

func testInventoryToOneSetOpRegionUsingRegion(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Inventory
	var b, c Region

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, inventoryDBTypes, false, strmangle.SetComplement(inventoryPrimaryKeyColumns, inventoryColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, regionDBTypes, false, strmangle.SetComplement(regionPrimaryKeyColumns, regionColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, regionDBTypes, false, strmangle.SetComplement(regionPrimaryKeyColumns, regionColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*Region{&b, &c} {
		err = a.SetRegion(ctx, tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.Region != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.Inventories[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if a.RegionID != x.ID {
			t.Error("foreign key was wrong value", a.RegionID)
		}

		zero := reflect.Zero(reflect.TypeOf(a.RegionID))
		reflect.Indirect(reflect.ValueOf(&a.RegionID)).Set(zero)

		if err = a.Reload(ctx, tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if a.RegionID != x.ID {
			t.Error("foreign key was wrong value", a.RegionID, x.ID)
		}
	}
}
func testInventoryToOneSetOpUserUsingSeller(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Inventory
	var b, c User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, inventoryDBTypes, false, strmangle.SetComplement(inventoryPrimaryKeyColumns, inventoryColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*User{&b, &c} {
		err = a.SetSeller(ctx, tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.Seller != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.SellerInventories[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if a.SellerID != x.ID {
			t.Error("foreign key was wrong value", a.SellerID)
		}

		zero := reflect.Zero(reflect.TypeOf(a.SellerID))
		reflect.Indirect(reflect.ValueOf(&a.SellerID)).Set(zero)

		if err = a.Reload(ctx, tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if a.SellerID != x.ID {
			t.Error("foreign key was wrong value", a.SellerID, x.ID)
		}
	}
}
func testInventoryToOneSetOpUserUsingDropshipper(t *testing.T) {
	var err error

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()

	var a Inventory
	var b, c User

	seed := randomize.NewSeed()
	if err = randomize.Struct(seed, &a, inventoryDBTypes, false, strmangle.SetComplement(inventoryPrimaryKeyColumns, inventoryColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &b, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}
	if err = randomize.Struct(seed, &c, userDBTypes, false, strmangle.SetComplement(userPrimaryKeyColumns, userColumnsWithoutDefault)...); err != nil {
		t.Fatal(err)
	}

	if err := a.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}
	if err = b.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Fatal(err)
	}

	for i, x := range []*User{&b, &c} {
		err = a.SetDropshipper(ctx, tx, i != 0, x)
		if err != nil {
			t.Fatal(err)
		}

		if a.R.Dropshipper != x {
			t.Error("relationship struct not set to correct value")
		}

		if x.R.DropshipperInventories[0] != &a {
			t.Error("failed to append to foreign relationship struct")
		}
		if a.DropshipperID != x.ID {
			t.Error("foreign key was wrong value", a.DropshipperID)
		}

		zero := reflect.Zero(reflect.TypeOf(a.DropshipperID))
		reflect.Indirect(reflect.ValueOf(&a.DropshipperID)).Set(zero)

		if err = a.Reload(ctx, tx); err != nil {
			t.Fatal("failed to reload", err)
		}

		if a.DropshipperID != x.ID {
			t.Error("foreign key was wrong value", a.DropshipperID, x.ID)
		}
	}
}

func testInventoriesReload(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	if err = o.Reload(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testInventoriesReloadAll(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice := InventorySlice{o}

	if err = slice.ReloadAll(ctx, tx); err != nil {
		t.Error(err)
	}
}

func testInventoriesSelect(t *testing.T) {
	t.Parallel()

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	slice, err := Inventories().All(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if len(slice) != 1 {
		t.Error("want one record, got:", len(slice))
	}
}

var (
	inventoryDBTypes = map[string]string{`ID`: `int`, `ProductID`: `int`, `Quantity`: `int`, `CreatedBy`: `int`, `UpdatedBy`: `int`, `CreatedDate`: `timestamp`, `IsActive`: `tinyint`, `RegionID`: `int`, `SellerID`: `int`, `DropshipperID`: `int`}
	_                = bytes.MinRead
)

func testInventoriesUpdate(t *testing.T) {
	t.Parallel()

	if 0 == len(inventoryPrimaryKeyColumns) {
		t.Skip("Skipping table with no primary key columns")
	}
	if len(inventoryAllColumns) == len(inventoryPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Inventories().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	if rowsAff, err := o.Update(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("should only affect one row but affected", rowsAff)
	}
}

func testInventoriesSliceUpdateAll(t *testing.T) {
	t.Parallel()

	if len(inventoryAllColumns) == len(inventoryPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}

	seed := randomize.NewSeed()
	var err error
	o := &Inventory{}
	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryColumnsWithDefault...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Insert(ctx, tx, boil.Infer()); err != nil {
		t.Error(err)
	}

	count, err := Inventories().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Error("want one record, got:", count)
	}

	if err = randomize.Struct(seed, o, inventoryDBTypes, true, inventoryPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	// Remove Primary keys and unique columns from what we plan to update
	var fields []string
	if strmangle.StringSliceMatch(inventoryAllColumns, inventoryPrimaryKeyColumns) {
		fields = inventoryAllColumns
	} else {
		fields = strmangle.SetComplement(
			inventoryAllColumns,
			inventoryPrimaryKeyColumns,
		)
	}

	value := reflect.Indirect(reflect.ValueOf(o))
	typ := reflect.TypeOf(o).Elem()
	n := typ.NumField()

	updateMap := M{}
	for _, col := range fields {
		for i := 0; i < n; i++ {
			f := typ.Field(i)
			if f.Tag.Get("boil") == col {
				updateMap[col] = value.Field(i).Interface()
			}
		}
	}

	slice := InventorySlice{o}
	if rowsAff, err := slice.UpdateAll(ctx, tx, updateMap); err != nil {
		t.Error(err)
	} else if rowsAff != 1 {
		t.Error("wanted one record updated but got", rowsAff)
	}
}

func testInventoriesUpsert(t *testing.T) {
	t.Parallel()

	if len(inventoryAllColumns) == len(inventoryPrimaryKeyColumns) {
		t.Skip("Skipping table with only primary key columns")
	}
	if len(mySQLInventoryUniqueColumns) == 0 {
		t.Skip("Skipping table with no unique columns to conflict on")
	}

	seed := randomize.NewSeed()
	var err error
	// Attempt the INSERT side of an UPSERT
	o := Inventory{}
	if err = randomize.Struct(seed, &o, inventoryDBTypes, false); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	ctx := context.Background()
	tx := MustTx(boil.BeginTx(ctx, nil))
	defer func() { _ = tx.Rollback() }()
	if err = o.Upsert(ctx, tx, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert Inventory: %s", err)
	}

	count, err := Inventories().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}

	// Attempt the UPDATE side of an UPSERT
	if err = randomize.Struct(seed, &o, inventoryDBTypes, false, inventoryPrimaryKeyColumns...); err != nil {
		t.Errorf("Unable to randomize Inventory struct: %s", err)
	}

	if err = o.Upsert(ctx, tx, boil.Infer(), boil.Infer()); err != nil {
		t.Errorf("Unable to upsert Inventory: %s", err)
	}

	count, err = Inventories().Count(ctx, tx)
	if err != nil {
		t.Error(err)
	}
	if count != 1 {
		t.Error("want one record, got:", count)
	}
}
