
DROP PROCEDURE IF EXISTS  `get_service_fee`;
DELIMITER $$

CREATE PROCEDURE `get_service_fee` (
    IN p_declared_amount INTEGER,
    IN p_delivery_courier_id INTEGER,
    OUT _courier_name TEXT,
    OUT _standard_rate TEXT,
    OUT _cop_rate TEXT,
    OUT _cod_rate TEXT
)
BEGIN
    DECLARE service_fee INTEGER;
    DECLARE is_max_price BOOL;

    -- Get courier ID
    SET @name = (SELECT NAME FROM delivery_courier WHERE id = p_delivery_courier_id);

    -- Guard not found
    IF @name IS NULL THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Delivery courier id invalid error in get_service_fee';
    END IF;

    -- Get max value
    -- Determine max value
    SELECT
        price_end
    FROM delivery_courier_rate d
    WHERE true
        AND d.delivery_courier_id = p_delivery_courier_id
        AND d.is_max = 1
    INTO @max_price;

    IF p_declared_amount >= @max_price THEN
        SET is_max_price = true;
    END IF;

    IF is_max_price THEN
        SELECT
            @name,
            standard_rate,
            cop_rate,
            cod_rate
        FROM delivery_courier_rate
        WHERE TRUE
          AND is_max = 1
          AND delivery_courier_id = p_delivery_courier_id
        INTO _courier_name, _standard_rate, _cop_rate, _cod_rate;
    ELSE
        SELECT
            @name,
            standard_rate,
            cop_rate,
            cod_rate
        FROM delivery_courier_rate
        WHERE TRUE
          AND p_declared_amount BETWEEN price_start AND price_end
          AND delivery_courier_id = p_delivery_courier_id
        INTO _courier_name, _standard_rate, _cop_rate, _cod_rate;
    END IF;

    SET service_fee = 1;

END $$

DELIMITER;

