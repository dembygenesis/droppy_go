DROP FUNCTION IF EXISTS  `get_service_fee`;
DELIMITER $$

CREATE FUNCTION `get_service_fee` (
    p_declared_amount INTEGER,
    p_delivery_courier_id INTEGER
) RETURNS INTEGER DETERMINISTIC
BEGIN
    DECLARE service_fee INTEGER;

    -- Get courier ID
    SET @name = (SELECT name FROM delivery_courier WHERE id = p_delivery_courier_id);

    -- Guard not found
    IF @name IS NULL THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Delivery courier id invalid error in get_service_fee';
    END IF;

    -- GoGo express
    IF @name = 'GoGo Express' THEN
        SELECT
            cop_rate,
            cod_rate
        FROM delivery_courier_rate
        WHERE true
            AND is_max = 1
            AND id = p_delivery_courier_id
        INTO @max_cop_rate, @max_cod_rate;
    ELSEIF @name = 'LBC' THEN
        SELECT ' I am at LBC ';
    END IF;

    SET service_fee = 1;

    RETURN service_fee;
END $$

DELIMITER;

