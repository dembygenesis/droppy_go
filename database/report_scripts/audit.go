package main

import (
	"droppy_go/utilities/database_utils"
	"droppy_go/utilities/date"
	"droppy_go/utilities/file"
	string2 "droppy_go/utilities/string"
	"fmt"
	"os"
	"regexp"
)

/**
This is for running audit scripts
- Month by month, and item by item
 */


// GetAuditReportByMonth fetches all
func GetAuditReportByMonth(s, e string) {
	// Run query
	sql := `
		SELECT
		  DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') AS date,
		  d.declared_amount AS amount,
		  d.tracking_number,
		  d.id AS tran_num,
		  d.amount_distributor AS distributor_amount,
		  ds.name AS delivery_status,
		  do.name AS delivery_option,
		  r.name AS region,
		  CONCAT(u.lastname, ', ', u.firstname) AS seller_name,
		  GROUP_CONCAT(CONCAT(p.name, '-', dd.quantity)) AS items
		FROM
		  delivery d
		  INNER JOIN delivery_status ds
			ON 1 = 1
			AND d.delivery_status_id = ds.id
		  INNER JOIN delivery_option do
			ON 1 = 1
			AND d.delivery_option_id = do.id
		  INNER JOIN region r
			ON 1 = 1
			AND d.region_id = r.id
		  INNER JOIN delivery_detail dd
			ON 1 = 1
			AND dd.delivery_id = d.id
		  INNER JOIN product p
			ON 1 = 1
			AND dd.product_id = p.id
		  INNER JOIN user u
			ON 1 = 1
			AND d.seller_id = u.id
		WHERE 1 = 1
		  AND d.is_active = 1
		  AND ds.name IN ('Delivered', 'Returned')
		  AND d.created_date BETWEEN 'start_date 00:00' AND 'end_date 23:59'
		GROUP BY d.id
		ORDER BY d.created_date ASC
	`

	// Start date
	reStartDate := regexp.MustCompile("start_date")
	sql = reStartDate.ReplaceAllString(sql, s)

	// End date
	reEndDate := regexp.MustCompile("end_date")
	sql = reEndDate.ReplaceAllString(sql, e)

	fmt.Println("sql", sql)

	/**
	DB_HOST=private-droppy-do-user-2049335-0.a.db.ondigitalocean.com
	DB_USER=doadmin
	DB_PASSWORD=galwickyshy7taz1
	DB_DATABASE=droppy
	DB_PORT=25060
	 */

	// Get results as csv string
	db, err := database_utils.GetSQLInstance(
		`droppy-do-user-2049335-0.a.db.ondigitalocean.com`,
		`droppy`,
		`mep1r1gnoxrlhpw2`,
		`25060`,
		"droppy",

		/*"localhost",
		"root",
		"rootPassword",
		"3357",
		"droppy",*/
	)
	defer db.Close()
	if err != nil {
		panic("Error trying to start the database: " + err.Error())
	}

	// ==========================================
	sqlCount := "SELECT COUNT(*) FROM (" + sql + ") AS a"
	var sqlRowCount int
	err = db.QueryRow(sqlCount).Scan(&sqlRowCount)
	if err != nil {
		panic("SQLoutside Row Count: " + err.Error())
	}
	// ==========================================

	// Build sql data into [][]string
	queryResults, err := database_utils.GetRowContentsAsMultiStringArr(sql, db)
	if err != nil {
		panic("Error trying to fetch rows: " + err.Error())
	}

	fileName := "./output/audit-report-" + s + "-" + e + ".csv"

	// Create file
	fileExists := file.FileExists(fileName)
	if fileExists {
		// Skip
		fmt.Println("Skipping file already alive: " + fileName)
		return
	}

	if len(*queryResults) == 1 {
		fmt.Println("Dont bother writing if no results")
		return
	}

	f, err := os.Create(fileName)
	defer f.Close()
	if err != nil {
		panic("Error trying to create a new file: " + err.Error())
	}

	// Write to file
	_, err = f.WriteString(string2.MultiDimensionalStringArrayToCSVText(*queryResults))
	if err != nil {
		panic("errors writing to the CSV: " + err.Error())
	}
}

func main() {
	// GetAuditReportByMonth()
	r := date.GetRanges("2021-08-01", "2021-08-31")

	l := 5
	c := make(chan bool, l)

	for _, v := range r {
		c <- true
		go func() {
			fmt.Println("v.Start", v.Start)
			fmt.Println("v.End", v.End)

			GetAuditReportByMonth(v.Start, v.End)
			<- c
		}()
	}

	for i := 0; i < cap(c); i++ {
		c <- true
	}
}