-- GoGo Express
SET @delivery_courier_id = (SELECT id
                            FROM delivery_courier
                            WHERE `name` = "GoGo Express");

INSERT INTO delivery_courier_rate (delivery_courier_id,
                                   price_start,
                                   price_end,
                                   is_max,
                                   standard_rate)

VALUES (@delivery_courier_id, 0, 1499, 0, 195),
       (@delivery_courier_id, 1500, 1999, 0, 200),
       (@delivery_courier_id, 2000, 2499, 0, 205),
       (@delivery_courier_id, 2500, 2999, 0, 240),
       (@delivery_courier_id, 3000, 3499, 0, 245),
       (@delivery_courier_id, 3500, 3999, 0, 250),
       (@delivery_courier_id, 4000, 4499, 0, 255),
       (@delivery_courier_id, 4500, 4999, 1, 260);

-- LBC
SET @delivery_courier_id = (SELECT id
                            FROM delivery_courier
                            WHERE `name` = "LBC");

INSERT INTO delivery_courier_rate (delivery_courier_id,
                                   price_start,
                                   price_end,
                                   is_max,
                                   cod_rate,
                                   cop_rate
                                   )
VALUES (@delivery_courier_id, 0,    999,  0, 150, 220),
       (@delivery_courier_id, 1000, 1499, 0, 155, 225),
       (@delivery_courier_id, 1500, 1999, 0, 160, 230),
       (@delivery_courier_id, 2000, 2499, 0, 165, 235),
       (@delivery_courier_id, 2500, 2999, 0, 170, 240),
       (@delivery_courier_id, 3000, 3499, 0, 175, 245),
       (@delivery_courier_id, 3500, 3999, 0, 180, 250),
       (@delivery_courier_id, 4000, 4499, 0, 185, 255),
       (@delivery_courier_id, 4500, 4999, 0, 190, 260),
       (@delivery_courier_id, 5000, 5499, 0, 195, 265),
       (@delivery_courier_id, 5500, 5999, 0, 200, 270),

       (@delivery_courier_id, 6000, 6499, 0, 205, 275),
       (@delivery_courier_id, 6500, 6999, 0, 210, 280),
       (@delivery_courier_id, 7000, 7499, 0, 215, 285),
       (@delivery_courier_id, 7500, 7999, 0, 220, 290),
       (@delivery_courier_id, 8000, 8499, 0, 225, 295),
       (@delivery_courier_id, 8500, 8999, 0, 230, 300),
       (@delivery_courier_id, 9000, 9499, 0, 235, 305),
       (@delivery_courier_id, 9500, 9999, 1, 240, 310);
