/**
  Deliveries

  Date- Customer Name-address-Amount-Additional fee (if applicable)-Delivery Status
 */

USE droppy;

SET @start_date = "2021-10-01";
SET @end_date = "2021-10-30";

SELECT
    IF(DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "",
       DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
    d.`name` AS `customer`,
    d.`address`,
    d.`declared_amount` AS amount,
    d.`amount_distributor` AS distributor_amount,
    ds.`name` AS delivery_status,
    do.`name` AS delivery_option
FROM
    delivery d
        INNER JOIN delivery_status ds
                   ON 1 = 1
                       AND d.`delivery_status_id` = ds.`id`
        INNER JOIN delivery_option `do`
                   ON 1 = 1
                       AND d.`delivery_option_id` = do.`id`
WHERE 1 = 1
  AND d.`is_active` = 1
  AND d.`created_date` BETWEEN CONCAT(@start_date, ' 00:00') AND CONCAT(@end_date, ' 23:59')
ORDER BY d.`created_date` ASC;

/**
  Withdrawal
 */

SELECT
    IF(DATE_FORMAT(CONVERT_TZ(w.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "",
       DATE_FORMAT(CONVERT_TZ(w.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
    CONCAT(u.`lastname`, ', ', u.`firstname`) AS customer_name,
    w.`bank_account_name` AS bank_name_sent,
    w.`amount`,
    w.`reference_number`
FROM
    withdrawal w
        INNER JOIN `user` u
                   ON 1 = 1
                       AND w.`user_id` = u.`id`
WHERE 1 = 1
  AND w.`created_date` BETWEEN CONCAT(@start_date, ' 00:00') AND CONCAT(@end_date, ' 23:59')
  AND w.`is_active` = 1;

/**
  Payins
  Date- customer Name-amount-pin #-amount-status
 */

SELECT
    IF(DATE_FORMAT(CONVERT_TZ(o.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "",
       DATE_FORMAT(CONVERT_TZ(o.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
    CONCAT(u.`lastname`, ', ', u.`firstname`) AS customer_name,
    u.`m88_account`,
    o.`amount`,
    os.`name` AS `status`
FROM
    `order` o
        INNER JOIN `user` u
                   ON 1 = 1
                       AND o.`seller_id` = u.`id`
        INNER JOIN order_status os
                   ON 1 = 1
                       AND o.`order_status_id` = os.`id`
WHERE 1 = 1
  AND o.`created_date` BETWEEN CONCAT(@start_date, ' 00:00') AND CONCAT(@end_date, ' 23:59')
  AND o.`is_active` = 1;


/**
  Top Sales
 */

SET @iterator = 0;

SELECT
    @iterator := @iterator + 1 AS number,
  seller,
  number_of_sales,
  total_declared_amount,
  total_amount_distributor,
  profit
FROM
    (SELECT
    CONCAT(u.lastname, ', ', u.firstname) AS seller,
    COUNT(DISTINCT (d.id)) AS number_of_sales,
    SUM(d.declared_amount) AS total_declared_amount,
    SUM(d.amount_distributor) AS total_amount_distributor,
    SUM(d.declared_amount) - SUM(d.amount_distributor) AS profit
    FROM
    `delivery` d
    INNER JOIN `user` u
    ON 1 = 1
    AND d.seller_id = u.id
    WHERE TRUE
    AND d.is_active = 1
    AND d.created_date BETWEEN @start_date
    AND @end_date
    AND d.delivery_status_id =
    (SELECT
    id
    FROM
    delivery_status
    WHERE `name` = 'Delivered')
    AND d.declared_amount > d.amount_distributor
    GROUP BY d.seller_id
    ORDER BY number_of_sales DESC
    LIMIT 20) AS a;

/**
  Top Packages
 */



SELECT
    CONCAT(u.lastname, ', ', u.firstname) AS seller,
    COUNT(DISTINCT (o.id)) AS number_of_orders
FROM
    `order` o
        INNER JOIN `user` u
                   ON 1 = 1
                       AND o.seller_id = u.id
WHERE TRUE
  AND o.is_active = 1
  AND o.created_date BETWEEN @start_date
    AND @end_date
GROUP BY o.seller_id
ORDER BY number_of_orders DESC
    LIMIT 20;

