/**
  When the admin view it, it SHOULD MAKE SENSE:
  1. Oh I gave 1000 to demby (seller)
  2. Oh I gave 35 to alex (dropshipper)
  3. oh I have 50 (withdrawals fee) to (seller) or (dropshipper)
  4. Oh I gave
 */

-- Goal #1: Dropshipper +35 payments , +75 orders, and + XXX withdrawal
SELECT
    ct.id AS coin_transaction_id,
    CONCAT(u.`lastname`, ', ', u.`firstname`) AS user_fullname,
    ut.name AS user_type,
    ct.amount * - 1 AS amount,
    CASE
        WHEN (ct.order_id IS NOT NULL)
            THEN "order"
        WHEN (ct.delivery_id IS NOT NULL)
            THEN "delivery"
        WHEN (ct.withdrawal_id IS NOT NULL)
            THEN "withdrawal"
        ELSE "unknown"
        END AS transaction_type
FROM
    coin_transaction ct
        INNER JOIN `user` u
                   ON 1 = 1
                       AND ct.`user_id` = u.`id`
        INNER JOIN user_type ut
                   ON 1 = 1
                       AND u.`user_type_id` = ut.`id`
WHERE 1 = 1
  AND ct.`is_active` = 1
  AND ut.`name` IN ("Dropshipper")
ORDER BY ct.created_date DESC;


-- Goal #2: Sellers, Dropshipper
-- Seller
-- Add
-- Seller
-- Add
SELECT
    ct.id AS coin_transaction_id,
    CONCAT(u.`lastname`, ', ', u.`firstname`) AS user_fullname,
    ut.name AS user_type,
    ct.type,
    IF (ct.type = 'D', ABS(ct.amount) * -1, ABS(ct.amount)) AS amount_type,
    CASE
        -- Seller: Order Logic 1
        WHEN (ct.order_id IS NOT NULL AND ut.name = "Seller") THEN "Seller Order"

        -- Dropshipper: Order Logic 2
        WHEN (ct.order_id IS NOT NULL AND ut.name = "Dropshipper") THEN "Credit Dropshipper Package Fee"

        -- Seller: Delivery logic 1
        WHEN (ct.delivery_id IS NOT NULL AND ct.amount * - 1 > 0 AND ut.name = "Seller") THEN "Delivery Success"

        -- Seller: Delivery logic 2
        WHEN (ct.delivery_id IS NOT NULL AND ct.amount * - 1 < 0 AND ut.name = "Seller") THEN "Delivery Attempt"

        -- Dropshipper: Delivery Logic 1
        WHEN (ct.delivery_id IS NOT NULL AND ut.name = "Dropshipper" AND ABS(ct.amount) IN (75, 35)) THEN "Credit Dropshipper Handling Fee"

        -- Dropshipper: Delivery Logic 2
        WHEN (ct.delivery_id IS NOT NULL AND ut.name = "Dropshipper") THEN "Credit Dropshipper Dropship/Package"

        WHEN (ct.withdrawal_id IS NOT NULL)
            THEN "withdrawal"
        WHEN (
                ct.coin_transaction_id IS NOT NULL AND ct.amount * - 1 > 0
            )
            THEN "Money In"
        WHEN (
                ct.coin_transaction_id IS NOT NULL AND ct.amount * - 1 < 0
            )
            THEN "Money Out"
        ELSE "unknown"
        END AS transaction_type
FROM
    coin_transaction ct
        INNER JOIN `user` u
                   ON 1 = 1
                       AND ct.`user_id` = u.`id`
        INNER JOIN user_type ut
                   ON 1 = 1
                       AND u.`user_type_id` = ut.`id`
WHERE 1 = 1
  AND ct.`is_active` = 1
  AND ut.`name` IN ("Seller", "Dropshipper")
ORDER BY ct.created_date DESC