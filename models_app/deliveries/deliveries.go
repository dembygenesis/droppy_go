package deliveries

import (
	"database/sql"
	"droppy_go/database"
	"droppy_go/models"
	UserModel "droppy_go/models_app/users"
	"droppy_go/services/email"
	"droppy_go/services/users"
	"droppy_go/utilities/response_builder"
	"errors"
	"fmt"
	"os"
	"strconv"
)

type DeliveryDetail struct {
	Id             int    `json:"id" db:"id"`
	SellerId       int    `json:"seller_id" db:"seller_id"`
	DeliveryStatus string `json:"delivery_status" db:"delivery_status"`
}

func (d *Delivery) GetCouriers() (
	*models.DeliveryCourierSlice,
	error) {
	// var couriers []Courier
	tx, _ := database.DBInstancePublic.Begin()
	res, err := models.DeliveryCouriers().All(nil, tx)
	if err != nil {
		return nil, err
	}
	return &res, nil
}

func (d *Delivery) GetDetailsFromDeliveryId(i int) (*DeliveryDetail, error) {
	var deliveryDetail DeliveryDetail
	sql := `
		SELECT 
		    d.id,
			d.seller_id,
			ds.name AS delivery_status
		FROM delivery d
		INNER JOIN delivery_status ds 
			ON 1 =1 
				AND d.delivery_status_id = ds.id
		WHERE 1 = 1
			AND d.id = ?
	`
	err := database.DBInstancePublic.Get(&deliveryDetail, sql, i)
	if err != nil {
		return &deliveryDetail, err
	}
	return &deliveryDetail, nil
}

func (d *Delivery) UpdateDelivery(p ParamsUpdateDelivery) (*sql.Result, error) {

	var oldDeliveryStatus string
	deliveryDetails, err := d.GetDetailsFromDeliveryId(p.DeliveryId)
	if err != nil {
		return nil, errors.New("errors fetching delivery details: " + err.Error())
	}
	oldDeliveryStatus = deliveryDetails.DeliveryStatus

	sql := `CALL update_delivery(?, ?, ?, ?, ?);`

	res, err := database.DBInstancePublic.Exec(
		sql,
		d.UserId,
		p.DeliveryId,
		p.DeliveryStatus,
		p.TrackingNumber,
		p.VoidOrRejectReason,
	)

	if err != nil {
		return &res, err
	}

	go func() {
		if p.DeliveryStatus == "Proposed" {
			return
		}
		userDetail, err := users.GetUserDetails(deliveryDetails.SellerId)
		if err != nil {
			return
		}
		from := os.Getenv("EMAIL_FROM")
		to := userDetail.Email

		message := "Hi " + userDetail.FirstName + " " + userDetail.LastName + ",<br><br>"
		message += `Updated delivery #(<i>` + strconv.Itoa(p.DeliveryId) + `</i>) status from <i>` + oldDeliveryStatus + "</i> to <i>" + p.DeliveryStatus + "</i>.<br/><br/>"
		message += "Thanks :),<br/>Droppy"

		subject := "Updated delivery status"

		attachment := ""
		err = email.SendMail(
			from,
			to,
			subject,
			message,
			attachment,
		)
		if err != nil {
			fmt.Println("======== Error Sending Email! ========", err.Error())
		} else {
			fmt.Println("======== Sent Email! ========")
		}
	}()

	return &res, nil
}

func (d *Delivery) GetDetails() {
	// var responseDeliveryDetails ResponseDeliveryDetails
}

func (d *Delivery) CreateParcel(p *ParamsCreateParcel) (sql.Result, error) {
	sql := `
		CALL add_delivery(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	`

	res, err := database.DBInstancePublic.Exec(sql,
		p.DeliveryOption,
		p.SellerId,
		p.DropshipperId,
		p.Name,
		p.ContactNumber,
		p.Address,
		p.Note,
		p.Region,
		p.DeclaredAmount,
		p.DeliveryDetails,
		p.CourierId,
		p.CourierDeliveryType,
	)

	// Send mail

	return res, err
}

func (d *Delivery) CreatePackage(p ParamsCreateOrder) (sql.Result, error) {

	// These will change based on the emails LATER

	sql := `
		CALL add_order(?, ?, ?, ?)
	`

	res, err := database.DBInstancePublic.Exec(sql,
		p.SellerId,
		p.DropshipperId,
		p.OrderDetails,
		p.Region,
	)

	return res, err
}

func (d *Delivery) GetCoinTransactions(userId int, userType string, page int, rows int) (*[]ResponseCoinTransactions, response_builder.Pagination, error) {
	var container []ResponseCoinTransactions
	var pagination response_builder.Pagination

	sql := `
		SELECT 
		  date_created,		
		  ` + "`type`" + `,
		  amount,
		  reference_number,
		  bank_type,
		  source,
		  recipient,
		  tran_num,
		  is_active
		FROM (
		  (
		    SELECT
		      IF(DATE_FORMAT(CONVERT_TZ(t.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(t.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
		      IF (t.money_in = 1, 'Money In', 'Money Out') AS type, 
			  t.created_date,
		      t.amount,
		      t.reference_number,
		      bt.name AS bank_type,
			  'Cash In' AS source,
			  'N/A' AS recipient,
			  t.id AS tran_num,
			  t.is_active
		    FROM
		      ` + "`transaction`" + ` t
		      INNER JOIN bank_type bt
		        ON 1 =1
		          AND t.bank_type_id = bt.id
		      WHERE 1 = 1
		        AND t.user_allotted_id = ?
		        AND t.is_active = 1
			ORDER BY t.created_date DESC
		  )
		  UNION ALL 
		  (
		    SELECT
		      IF(DATE_FORMAT(CONVERT_TZ(ct.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(ct.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
		      IF(ct.type = 'C', 'Coins In', 'Coins Out') AS type,
			  ct.created_date,
		      ct.amount * -1 AS amount,
		      'N/A' AS reference_number,
		      'N/A' AS bank_type,
			  CASE
			    WHEN ct.withdrawal_id IS NOT NULL THEN 'Withdrawal'	
			    WHEN ct.delivery_id IS NOT NULL THEN 'Delivery'	
			    WHEN ct.order_id IS NOT NULL THEN 'Order'	
			    WHEN ct.coin_transaction_id IS NOT NULL THEN 'Coins added from Cash In'
              END AS source,
			  IF (d.id IS NULL, 'N/A', d.name) AS recipient,
			  CASE
			    WHEN ct.withdrawal_id IS NOT NULL THEN ct.withdrawal_id	
			    WHEN ct.delivery_id IS NOT NULL THEN ct.delivery_id	
			    WHEN ct.order_id IS NOT NULL THEN ct.order_id	
			    WHEN ct.coin_transaction_id IS NOT NULL THEN ct.coin_transaction_id
              END AS tran_num,
			  ct.is_active
		    FROM
		      coin_transaction ct
		      LEFT JOIN delivery d 
			    ON 1 = 1
					AND ct.delivery_id = d.id 
		      WHERE 1 = 1
		        AND ct.user_id = ?
		        AND ct.is_active = 1
			ORDER BY ct.created_date DESC
		  )
		) AS a
		ORDER BY created_date DESC
	`

	var err error
	var count int

	paginate := func() (*[]ResponseCoinTransactions, response_builder.Pagination, error) {
		count, err = database.GetQueryCount(sql, userId, userId)

		if err != nil {
			return &container, pagination, err
		}

		if count == 0 {
			return &container, pagination, nil
		}

		sql, pages, rowsPerPage, offset, page, totalCount, resultCount := database.GetPaginationDetails(
			sql,
			count,
			page,
			rows,
			1000,
		)

		pagination.SetData(rowsPerPage, offset, pages, rows, page, totalCount, resultCount)

		err := database.DBInstancePublic.Select(
			&container,
			sql,
			userId,
			userId,
		)

		if err != nil {
			return &container, pagination, err
		}

		return &container, pagination, nil
	}

	res, pagination, err := paginate()

	return res, pagination, err
}

func (d *Delivery) GetCoinTransactions2(userId int, userType string, page int, rows int) (*[]ResponseCoinTransactions, response_builder.Pagination, error) {
	var container []ResponseCoinTransactions
	var pagination response_builder.Pagination

	sql := `
		SELECT 
		  date_created,		
		  ` + "`type`" + `,
		  amount,
		  reference_number,
		  bank_type,
		  source,
		  recipient,
		  tran_num,
		  void_or_reject_reason,
		  CASE
            WHEN type IN ('Money In', 'Money Out') OR is_active = 0
            THEN @running_total
            ELSE @running_total := @running_total + amount
          END AS running_balance,
          created_date,
          source_type,
		  is_active
		FROM (
		  (
		    SELECT
		      IF(DATE_FORMAT(CONVERT_TZ(t.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(t.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
		      IF (t.money_in = 1, 'Money In', 'Money Out') AS type, 
			  t.created_date,
		      t.amount,
		      t.reference_number,
		      bt.name AS bank_type,
			  'Cash In' AS source,
			  'N/A' AS recipient,
			  t.id AS tran_num,
              'N/A' AS void_or_reject_reason,
              'N/A' AS source_type,
			  t.is_active
		    FROM
		      ` + "`transaction`" + ` t
		      INNER JOIN bank_type bt
		        ON 1 =1
		          AND t.bank_type_id = bt.id
		      WHERE 1 = 1
		        AND t.user_allotted_id = ?
		        
			ORDER BY t.created_date DESC
		  )
		  UNION ALL 
		  (
		    SELECT
		      IF(DATE_FORMAT(CONVERT_TZ(ct.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(ct.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
		      IF(ct.type = 'C', 'Coins In', 'Coins Out') AS type,
			  ct.created_date,
		      ct.amount * -1 AS amount,
		      'N/A' AS reference_number,
		      'N/A' AS bank_type,
			  CASE
			    WHEN ct.withdrawal_id IS NOT NULL THEN 'Withdrawal'	
			    WHEN ct.delivery_id IS NOT NULL THEN 'Delivery'	
			    WHEN ct.order_id IS NOT NULL THEN 'Order'	
			    WHEN ct.coin_transaction_id IS NOT NULL THEN 'Coins added from Cash In'
              END AS source,
			  IF (d.id IS NULL, 'N/A', d.name) AS recipient,
			  CASE
			    WHEN ct.withdrawal_id IS NOT NULL THEN ct.withdrawal_id	
			    WHEN ct.delivery_id IS NOT NULL THEN ct.delivery_id	
			    WHEN ct.order_id IS NOT NULL THEN ct.order_id	
			    WHEN ct.coin_transaction_id IS NOT NULL THEN ct.coin_transaction_id
              END AS tran_num,
			  CASE
			    WHEN ct.withdrawal_id IS NOT NULL THEN 'N/A'
			    WHEN ct.delivery_id IS NOT NULL THEN IF (d.void_or_reject_reason IS NULL, '', d.void_or_reject_reason)
			    WHEN ct.order_id IS NOT NULL THEN 'N/A'	
			    WHEN ct.coin_transaction_id IS NOT NULL THEN 'N/A'
              END AS void_or_reject_reason,
			  CASE
			    WHEN ct.delivery_id IS NOT NULL THEN ds.name
 			    ELSE 'N/A'
              END AS source_type,
			  ct.is_active
		    FROM
		      coin_transaction ct
		      LEFT JOIN delivery d 
			    ON 1 = 1
					AND ct.delivery_id = d.id
			  LEFT JOIN delivery_status ds 
			    ON 1 = 1
					AND d.delivery_status_id = ds.id
		      WHERE 1 = 1
		        AND ct.user_id = ?
		        
			ORDER BY ct.created_date DESC
		  )
		) AS a
		ORDER BY created_date ASC
	`

	var err error
	var count int

	paginate := func() (*[]ResponseCoinTransactions, response_builder.Pagination, error) {
		count, err = database.GetQueryCount(sql, userId, userId)

		if err != nil {
			return &container, pagination, err
		}

		if count == 0 {
			return &container, pagination, nil
		}

		sql, pages, rowsPerPage, offset, page, totalCount, resultCount := database.GetPaginationDetails(
			sql,
			count,
			page,
			rows,
			1000,
		)

		pagination.SetData(rowsPerPage, offset, pages, rows, page, totalCount, resultCount)

		/**
		Replace with transactions here
		*/

		sql = `
			SELECT * FROM (
				` + sql + `    
			) AS a
			ORDER BY created_date DESC
		`

		tx := database.DBInstancePublic.MustBegin()

		_, err := tx.Exec("SET @running_total = 0")

		if err != nil {
			return &container, pagination, errors.New("SET @running_total = 0 problem")
		}

		err = tx.Select(&container, sql, userId, userId)

		return &container, pagination, err
	}

	res, pagination, err := paginate()

	return res, pagination, err
}

// Should I add a new filter object? And validate? LOL... Maybe.
func (d *Delivery) GetTransactions(
	userId int,
	userType string,
	deliveryStatus string,
	search string,
	page int,
	rows int,
) (*[]ResponseTransactions, response_builder.Pagination, error) {
	var container []ResponseTransactions
	var pagination response_builder.Pagination

	// Handle admin search filters
	adminDeliverySearchFilters := "1 = 1"
	adminOrderSearchFilters := "1 = 1"

	sql := ""

	if search != "" {
		// adminSearchFilters = "d.id = " + search
		adminDeliverySearchFilters = "d.id = " + search
		adminOrderSearchFilters = "o.id = " + search
	}

	// ADMINS
	if userType == "Admin" {
		sql = `
			SELECT 
			  date_created,
			  recipient,
			  transaction_number,
			  CAST(amount AS DECIMAL(65,2)) AS amount,
			  tracking_number,
			  ` + "`type`" + `,  
			  ` + "`status`" + `,
			  region,
			  seller,
			  dropshipper,
			  IF(delivery_courier IS NULL, "", delivery_courier) AS delivery_courier,
			  IF(delivery_courier_delivery_type IS NULL, "", delivery_courier_delivery_type) AS delivery_courier_delivery_type
			FROM (
				(
					SELECT 
                      d.created_date,
					  IF(DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
					  d.name AS recipient,
					  d.id AS transaction_number,
					  d.declared_amount AS amount,
					  IF(d.tracking_number IS NULL, "", d.tracking_number) AS tracking_number,
					  do.name AS type,
					  ds.name AS status,
 					  CONCAT(u_dropshipper.lastname, ', ', u_dropshipper.firstname) AS dropshipper,
 					  CONCAT(u_seller.lastname, ', ', u_seller.firstname) AS seller,
					  r.name AS region,
 					  (SELECT COUNT(id) FROM delivery_detail WHERE delivery_id = d.id) AS items,
					  dc.name AS delivery_courier,
					  dc_dt.name AS delivery_courier_delivery_type
					FROM delivery d
					INNER JOIN delivery_status ds 
					  ON 1 = 1
						AND d.delivery_status_id = ds.id 
					INNER JOIN delivery_option do
					  ON 1 = 1 
						AND d.delivery_option_id = do.id
					INNER JOIN ` + "`user`" + ` u_dropshipper
 					  ON 1 = 1
						AND d.dropshipper_id = u_dropshipper.id
					INNER JOIN ` + "`user`" + ` u_seller
 					  ON 1 = 1
						AND d.seller_id = u_seller.id
				    INNER JOIN region r
 					  ON 1 = 1
						AND d.region_id = r.id
					LEFT JOIN delivery_courier dc
 					  ON 1 = 1
						AND d.delivery_courier_id = dc.id
					LEFT JOIN delivery_courier_delivery_type dc_dt
 					  ON 1 = 1
						AND d.delivery_courier_delivery_type_id = dc_dt.id
					WHERE 1 = 1
					 AND (d.is_active = 1 OR 1 = 1)
					 AND ` + adminDeliverySearchFilters + `
				)
				UNION ALL
				(
					SELECT
                      o.created_date,
					  IF(DATE_FORMAT(CONVERT_TZ(o.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(o.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
					  CONCAT(u.lastname, ', ', u.firstname) AS recipient,
					  o.id AS transaction_number,
					  o.amount,
					  'N/A' AS tracking_number,
					  'Package' AS type,
					  IF(o.is_active = 1, 'Procured', 'Voided') AS status,
					  CONCAT(u_dropshipper.lastname, ', ', u_dropshipper.firstname) AS dropshipper,					  
					  CONCAT(u.lastname, ', ', u.firstname) AS seller,
					  r.name AS region,
					  (SELECT COUNT(id) FROM order_detail WHERE order_id = o.id) AS items,
                      "" AS delivery_courier,
                      "" AS delivery_courier_delivery_type
					FROM
					  ` + "`order`" + ` o
					  INNER JOIN user u 
					  	ON 1 = 1
							AND o.seller_id = u.id
					  INNER JOIN user u_dropshipper 
					  	ON 1 = 1
							AND o.dropshipper_id = u_dropshipper.id
					  INNER JOIN region r
						ON 1 = 1
							AND o.region_id = r.id
					WHERE 1 = 1
					  AND (o.is_active = 1 OR 1 = 1)
					  AND ` + adminOrderSearchFilters + `
				)
			) AS a
			ORDER BY created_date DESC
		`
	}

	// adminDeliverySearchFilters
	//adminOrderSearchFilters

	// DROPSHIPPER
	if userType == "Dropshipper" {
		sql = `
			SELECT 
			  date_created,
			  recipient,
			  transaction_number,
			  CAST(amount AS DECIMAL(65,2)) AS amount,
			  tracking_number,
			  ` + "`type`" + `,  
			  ` + "`status`" + `,
              IF(delivery_courier IS NULL, "", delivery_courier) AS delivery_courier,
              IF(delivery_courier_delivery_type IS NULL, "", delivery_courier_delivery_type) AS delivery_courier_delivery_type
			FROM (
				(
					SELECT 
                      d.created_date,
					  IF(DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
					  d.name AS recipient,
					  d.id AS transaction_number,
					  d.declared_amount AS amount,
					  IF(d.tracking_number IS NULL, "", d.tracking_number) AS tracking_number,
					  do.name AS type,
					  ds.name AS status,
					  dc.name AS delivery_courier,
					  dc_dt.name AS delivery_courier_delivery_type
					FROM delivery d
					INNER JOIN delivery_status ds 
					  ON 1 = 1
						AND d.delivery_status_id = ds.id 
					INNER JOIN delivery_option do
					  ON 1 = 1 
						AND d.delivery_option_id = do.id
					LEFT JOIN delivery_courier dc
 					  ON 1 = 1
						AND d.delivery_courier_id = dc.id
					LEFT JOIN delivery_courier_delivery_type dc_dt
 					  ON 1 = 1
						AND d.delivery_courier_delivery_type_id = dc_dt.id
					WHERE 1 = 1
					 AND d.dropshipper_id = ?
				)
				UNION ALL
				(
					SELECT
                      o.created_date,
					  IF(DATE_FORMAT(CONVERT_TZ(o.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(o.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
					  CONCAT(u.lastname, ', ', u.firstname) AS recipient,
					  o.id AS transaction_number,
					  o.amount,
					  'N/A' AS tracking_number,
					  'Package' AS type,
					  'Procured' AS status,
					  "" AS delivery_courier,
					  "" AS delivery_courier_delivery_type
					FROM
					  ` + "`order`" + ` o
					  INNER JOIN user u 
					  ON 1 = 1
						AND o.seller_id = u.id
					WHERE 1 = 1
					  AND o.dropshipper_id = ?
				)
			) AS a
			ORDER BY created_date DESC
		`
	}

	// SELLER
	if userType == "Seller" {
		// Do nothing for now lol
		sql = `
			SELECT 
			  date_created,
			  recipient,
			  transaction_number,
			  CAST(amount AS DECIMAL(65,2)) AS amount,
			  tracking_number,
			  ` + "`type`" + `,  
			  ` + "`status`" + `,
              IF(delivery_courier IS NULL, "", delivery_courier) AS delivery_courier,
              IF(delivery_courier_delivery_type IS NULL, "", delivery_courier_delivery_type) AS delivery_courier_delivery_type
			FROM (
				(
					SELECT 
     				  d.created_date,
					  IF(DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
					  d.name AS recipient,
					  d.id AS transaction_number,
					  d.declared_amount AS amount,
					  IF(d.tracking_number IS NULL, "", d.tracking_number) AS tracking_number,
					  do.name AS type,
					  ds.name AS status,
					  dc.name AS delivery_courier,
					  dc_dt.name AS delivery_courier_delivery_type
					FROM delivery d
					INNER JOIN delivery_status ds 
					  ON 1 = 1
						AND d.delivery_status_id = ds.id 
					INNER JOIN delivery_option do
					  ON 1 = 1 
						AND d.delivery_option_id = do.id
					LEFT JOIN delivery_courier dc
 					  ON 1 = 1
						AND d.delivery_courier_id = dc.id
					LEFT JOIN delivery_courier_delivery_type dc_dt
 					  ON 1 = 1
						AND d.delivery_courier_delivery_type_id = dc_dt.id
					WHERE 1 = 1
					 AND d.seller_id = ?
				)
				UNION ALL
				(
					SELECT
					  o.created_date,
					  IF(DATE_FORMAT(CONVERT_TZ(o.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(o.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
					  CONCAT(u.lastname, ', ', u.firstname) AS recipient,
					  o.id AS transaction_number,
					  o.amount,
					  'N/A' AS tracking_number,
					  'Package' AS type,
					  'Procured' AS status,
					  "" AS delivery_courier,
					  "" AS delivery_courier_delivery_type
					FROM
					  ` + "`order`" + ` o
					  INNER JOIN user u 
					  ON 1 = 1
						AND o.seller_id = u.id
					WHERE 1 = 1
					  AND o.seller_id = ?
				)
			) AS a
			ORDER BY created_date DESC
		`
	}

	// RIDER

	/**
	Fetches the transactions that are "Same Day Delivery"
	*/
	if userType == "Rider" {
		sql = `
			SELECT 
			  date_created,
			  recipient,
			  transaction_number,
			  CAST(amount AS DECIMAL(65,2)) AS amount,
			  tracking_number,
			  ` + "`type`" + `,  
			  ` + "`status`" + `,
			  address,
			  contact_number,
			  items
			FROM (
				(
					SELECT 
     				  d.created_date,
					  IF(DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
					  d.name AS recipient,
					  d.id AS transaction_number,
					  d.declared_amount AS amount,
					  IF(d.tracking_number IS NULL, "", d.tracking_number) AS tracking_number,
					  do.name AS type,
					  ds.name AS status,
					  d.address,
					  d.contact_number,
 					  (SELECT COUNT(id) FROM delivery_detail WHERE delivery_id = d.id) AS items
					FROM delivery d
					INNER JOIN delivery_status ds 
					  ON 1 = 1
						AND d.delivery_status_id = ds.id 
					INNER JOIN delivery_option do
					  ON 1 = 1 
						AND d.delivery_option_id = do.id
					WHERE 1 = 1
					 AND ds.name = ?
					 AND d.is_active = 1
					 AND d.region_id = ?
				)
			) AS a
			ORDER BY created_date DESC
		`
	}

	var err error
	var count int

	/**
	Pagination Logic
	*/

	paginate := func() (*[]ResponseTransactions, response_builder.Pagination, error) {

		user := UserModel.User{ID: userId}

		res, err := user.GetOne()

		if userType == "Admin" {
			count, err = database.GetQueryCount(sql)
		} else {
			if userType == "Rider" {
				count, err = database.GetQueryCount(sql, deliveryStatus, res[0].RegionId)
			} else {
				count, err = database.GetQueryCount(sql, userId, userId)
			}
		}

		// Fail error if error
		if err != nil {
			return &container, pagination, err
		}

		// Just return blank entries if there's no count (save operations)
		if count == 0 {
			return &container, pagination, nil
		}

		sql, pages, rowsPerPage, offset, page, totalCount, resultCount := database.GetPaginationDetails(
			sql,
			count,
			page,
			rows,
			1000,
		)

		pagination.SetData(rowsPerPage, offset, pages, rows, page, totalCount, resultCount)

		// Perform query
		if userType == "Admin" {
			err = database.DBInstancePublic.Select(&container, sql)
		} else {

			if userType == "Rider" {
				err = database.DBInstancePublic.Select(&container, sql, deliveryStatus, res[0].RegionId)
			} else {
				err = database.DBInstancePublic.Select(&container, sql, userId, userId)
			}
		}

		if err != nil {
			return &container, pagination, err
		}

		return &container, pagination, nil
	}

	res, pagination, err := paginate()

	return res, pagination, err
}

func (d *Delivery) GetServiceFee(orderDetails string) (*ResponseServiceFee, error) {
	var responseServiceFee ResponseServiceFee

	sql := `CALL get_service_fee(?)`

	err := database.DBInstancePublic.Get(&responseServiceFee, sql, orderDetails)

	return &responseServiceFee, err
}

func (d *Delivery) GetDeliveryDetails(userId int, userType string) (ResponseDeliveryDetails, error) {
	var responseDeliveryDetails ResponseDeliveryDetails

	var responseDeliveryDetailsItems []ResponseDeliveryDetailsItems
	var responseDeliveryDetailsInfo ResponseDeliveryDetailsInfo
	var responseDeliveryDetailsTracking []ResponseDeliveryDetailsTracking

	sqlDeliveryItems := ""
	sqlDeliveryInfo := ""
	sqlDeliveryTracking := ""

	if userType == "Seller" {
		sqlDeliveryItems = `
			SELECT
			  p.name AS product,
			  pt.name AS category,
			  dd.quantity
			FROM
			  delivery d
			  INNER JOIN delivery_detail dd
				ON 1 = 1
				AND d.id = dd.delivery_id
			  INNER JOIN product p 
				ON 1 = 1
				  AND dd.product_id = p.id
			  INNER JOIN product_type pt
				ON 1 = 1
				  AND p.product_type_id = pt.id
			WHERE 1 = 1
			  AND d.id = ?
			  AND d.seller_id = ?
			  AND d.is_active = 1
			  AND p.is_active = 1
		`

		err := database.DBInstancePublic.Select(&responseDeliveryDetailsItems, sqlDeliveryItems, d.ID, userId)

		if err != nil {
			return responseDeliveryDetails, err
		}

		sqlDeliveryInfo = `
			SELECT
			  DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') AS date_created,
			  r.name AS region,
			  CONCAT(u.lastname, ', ', u.firstname) AS dropshipper,
			  d.address,
			  d.declared_amount,
			  d.service_fee,
			  IF(d.tracking_number IS NULL, "", d.tracking_number) AS tracking_number,
			  d.contact_number,
			  d.note,
			  u2.mobile_number AS seller_mobile_number,
			  d.base_price,
			  d.name AS buyer_name,
			  u2.m88_account AS seller_m88_account,
			  CONCAT(u2.lastname, ', ', u2.firstname) AS seller_name
			FROM
			  delivery d
			  INNER JOIN region r 
				ON 1 = 1
				  AND d.region_id = r.id
			  INNER JOIN user u 
				ON 1 = 1
				  AND d.dropshipper_id = u.id
			  INNER JOIN user u2 
				ON 1 = 1
				  AND d.seller_id = u2.id
			WHERE 1 = 1
			  AND d.id = ?
			  AND d.seller_id = ?
			  AND d.is_active = 1
			  AND u.is_active = 1	
		`

		err = database.DBInstancePublic.Get(&responseDeliveryDetailsInfo, sqlDeliveryInfo, d.ID, userId)

		fmt.Println(responseDeliveryDetailsInfo)

		if err != nil {
			return responseDeliveryDetails, err
		}

		sqlDeliveryTracking = `
			SELECT
			  DATE_FORMAT(
				CONVERT_TZ(dt.last_updated,'+00:00','+08:00'),
				'%Y-%m-%d %h:%i %p'
			  ) AS date_created,
			  ds.name AS status
			FROM
			  delivery d
			  INNER JOIN delivery_tracking dt
				ON 1 = 1
				AND d.id = dt.delivery_id
			  INNER JOIN delivery_status ds
				ON 1 = 1
				AND dt.delivery_status_id = ds.id
			WHERE 1 = 1
			  AND d.id = ?
			  AND d.seller_id = ?
			  AND d.is_active = 1
			  AND d.is_active = 1		  
			ORDER BY d.created_date DESC
		`

		err = database.DBInstancePublic.Select(&responseDeliveryDetailsTracking, sqlDeliveryTracking, d.ID, userId)

		if err != nil {
			return responseDeliveryDetails, err
		}

		// Build output
		responseDeliveryDetails.Info = &responseDeliveryDetailsInfo
		responseDeliveryDetails.Items = &responseDeliveryDetailsItems
		responseDeliveryDetails.Tracking = &responseDeliveryDetailsTracking
	}

	if userType == "Dropshipper" {
		sqlDeliveryItems = `
			SELECT
			  p.name AS product,
			  pt.name AS category,
			  dd.quantity
			FROM
			  delivery d
			  INNER JOIN delivery_detail dd
				ON 1 = 1
				AND d.id = dd.delivery_id
			  INNER JOIN product p 
				ON 1 = 1
				  AND dd.product_id = p.id
			  INNER JOIN product_type pt
				ON 1 = 1
				  AND p.product_type_id = pt.id
			WHERE 1 = 1
			  AND d.id = ?
			  AND d.dropshipper_id = ?
			  AND d.is_active = 1
			  AND p.is_active = 1
		`

		err := database.DBInstancePublic.Select(&responseDeliveryDetailsItems, sqlDeliveryItems, d.ID, userId)

		if err != nil {
			return responseDeliveryDetails, err
		}

		sqlDeliveryInfo = `
			SELECT
			  DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') AS date_created,
			  r.name AS region,
			  CONCAT(u.lastname, ', ', u.firstname) AS dropshipper,
			  d.address,
			  d.declared_amount,
			  d.service_fee,
			  d.contact_number,
			  d.note,
			  IF(d.tracking_number IS NULL, "", d.tracking_number) AS tracking_number,
			  u2.mobile_number AS seller_mobile_number,
			  d.base_price,
			  d.name AS buyer_name,
			  u2.m88_account AS seller_m88_account,
			  CONCAT(u2.lastname, ', ', u2.firstname) AS seller_name
			FROM
			  delivery d
			  INNER JOIN region r 
				ON 1 = 1
				  AND d.region_id = r.id
			  INNER JOIN user u 
				ON 1 = 1
				  AND d.dropshipper_id = u.id
			  INNER JOIN user u2 
				ON 1 = 1
				  AND d.seller_id = u2.id
			WHERE 1 = 1
			  AND d.id = ?
			  AND d.dropshipper_id = ?
			  AND d.is_active = 1
			  AND u.is_active = 1	
		`

		err = database.DBInstancePublic.Get(&responseDeliveryDetailsInfo, sqlDeliveryInfo, d.ID, userId)

		if err != nil {
			return responseDeliveryDetails, err
		}

		sqlDeliveryTracking = `
			SELECT
			  DATE_FORMAT(
				CONVERT_TZ(dt.last_updated,'+00:00','+08:00'),
				'%Y-%m-%d %h:%i %p'
			  ) AS date_created,
			  ds.name AS status
			FROM
			  delivery d
			  INNER JOIN delivery_tracking dt
				ON 1 = 1
				AND d.id = dt.delivery_id
			  INNER JOIN delivery_status ds
				ON 1 = 1
				AND dt.delivery_status_id = ds.id
			WHERE 1 = 1
			  AND d.id = ?
			  AND d.dropshipper_id = ?
			  AND d.is_active = 1
			  AND d.is_active = 1		  
			ORDER BY d.created_date DESC
		`

		err = database.DBInstancePublic.Select(&responseDeliveryDetailsTracking, sqlDeliveryTracking, d.ID, userId)

		if err != nil {
			return responseDeliveryDetails, err
		}

		// Build output
		responseDeliveryDetails.Info = &responseDeliveryDetailsInfo
		responseDeliveryDetails.Items = &responseDeliveryDetailsItems
		responseDeliveryDetails.Tracking = &responseDeliveryDetailsTracking
	}

	if userType == "Admin" {
		sqlDeliveryItems = `
			SELECT
			  p.name AS product,
			  pt.name AS category,
			  dd.quantity
			FROM
			  delivery d
			  INNER JOIN delivery_detail dd
				ON 1 = 1
				AND d.id = dd.delivery_id
			  INNER JOIN product p 
				ON 1 = 1
				  AND dd.product_id = p.id
			  INNER JOIN product_type pt
				ON 1 = 1
				  AND p.product_type_id = pt.id
			WHERE 1 = 1
			  AND d.id = ?
			  AND d.is_active = 1
			  AND p.is_active = 1
		`

		err := database.DBInstancePublic.Select(&responseDeliveryDetailsItems, sqlDeliveryItems, d.ID)

		if err != nil {
			return responseDeliveryDetails, err
		}

		sqlDeliveryInfo = `
			SELECT
			  DATE_FORMAT(CONVERT_TZ(d.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') AS date_created,
			  r.name AS region,
			  CONCAT(u.lastname, ', ', u.firstname) AS dropshipper,
			  d.address,
			  d.declared_amount,
			  d.service_fee,
			  d.contact_number,
			  d.note,
			  IF(d.tracking_number IS NULL, "", d.tracking_number) AS tracking_number,
			  u2.mobile_number AS seller_mobile_number,
			  d.base_price,
			  d.name AS buyer_name,
			  u2.m88_account AS seller_m88_account,
			  CONCAT(u2.lastname, ', ', u2.firstname) AS seller_name
			FROM
			  delivery d
			  INNER JOIN region r 
				ON 1 = 1
				  AND d.region_id = r.id
			  INNER JOIN user u 
				ON 1 = 1
				  AND d.dropshipper_id = u.id
			  INNER JOIN user u2 
				ON 1 = 1
				  AND d.seller_id = u2.id
			WHERE 1 = 1
			  AND d.id = ?
			  AND d.is_active = 1
			  AND u.is_active = 1	
		`

		err = database.DBInstancePublic.Get(&responseDeliveryDetailsInfo, sqlDeliveryInfo, d.ID)

		if err != nil {
			return responseDeliveryDetails, err
		}

		sqlDeliveryTracking = `
			SELECT
			  DATE_FORMAT(
				CONVERT_TZ(dt.last_updated,'+00:00','+08:00'),
				'%Y-%m-%d %h:%i %p'
			  ) AS date_created,
			  ds.name AS status
			FROM
			  delivery d
			  INNER JOIN delivery_tracking dt
				ON 1 = 1
				AND d.id = dt.delivery_id
			  INNER JOIN delivery_status ds
				ON 1 = 1
				AND dt.delivery_status_id = ds.id
			WHERE 1 = 1
			  AND d.id = ?
			  AND d.is_active = 1
			  AND d.is_active = 1		  
			ORDER BY d.created_date DESC
		`

		err = database.DBInstancePublic.Select(&responseDeliveryDetailsTracking, sqlDeliveryTracking, d.ID)

		if err != nil {
			return responseDeliveryDetails, err
		}

		// Build output
		responseDeliveryDetails.Info = &responseDeliveryDetailsInfo
		responseDeliveryDetails.Items = &responseDeliveryDetailsItems
		responseDeliveryDetails.Tracking = &responseDeliveryDetailsTracking
	}

	if sqlDeliveryInfo == "" || sqlDeliveryItems == "" || sqlDeliveryTracking == "" {
		return responseDeliveryDetails, errors.New("no sql ran for GetDeliveryDetails")
	}

	return responseDeliveryDetails, nil
}

func Top10Sellers() (*[]Top10Seller, error) {
	var Top10Sellers []Top10Seller

	sql := `
		SELECT
		  d.seller_id,
		  CONCAT(u.lastname, ', ', u.firstname) AS seller,
		  COUNT(*) AS deliveries,
		  SUM(d.declared_amount) AS sales
		FROM
		  delivery d
		  INNER JOIN user u
			ON 1 = 1
			AND d.seller_id = u.id
		  INNER JOIN delivery_status ds
			 ON 1 = 1
			   AND d.delivery_status_id = ds.id
			   AND ds.name IN ('Fulfilled', 'Delivered')
		WHERE 1 = 1
		  AND d.created_date BETWEEN '2020-09-01' AND  '2020-09-30'
		GROUP BY u.id
		ORDER BY sales DESC, deliveries DESC
		LIMIT 10
	`

	err := database.DBInstancePublic.Select(&Top10Sellers, sql)

	return &Top10Sellers, err
}

func (d *Delivery) GetDashboardDeliveryStatus(userId int, userType string) (*ResponseDashboardDeliveryStatus, error) {
	var responseDashboardDeliveryStatusContainer []ResponseDashboardDeliveryStatusContainer
	var responseDashboardDeliveryStatus ResponseDashboardDeliveryStatus

	var sql string

	if userType == "Seller" {
		sql = `
			SELECT
			  ds.name,
			  IF (a.amount IS NULL, 0, a.amount) AS amount
			FROM
			  delivery_status ds
			  LEFT JOIN
				(SELECT
				  IF(COUNT(*) IS NULL, 0, COUNT(*)) AS amount,
				 
				  ds.name AS delivery_status
				FROM
				  delivery d
				  INNER JOIN delivery_status ds
					ON 1 = 1
					AND d.delivery_status_id = ds.id
				WHERE 1 = 1
				  AND d.is_active = 1
				  AND d.seller_id = ?
				  GROUP BY ds.name) a
				ON 1 = 1
				AND a.delivery_status = ds.name
		`
	}

	if userType == "Dropshipper" {
		sql = `
			SELECT
			  ds.name,
			  IF (a.amount IS NULL, 0, a.amount) AS amount
			FROM
			  delivery_status ds
			  LEFT JOIN
				(SELECT
				  IF(COUNT(*) IS NULL, 0, COUNT(*)) AS amount,
				  ds.name AS delivery_status
				FROM
				  delivery d
				  INNER JOIN delivery_status ds
					ON 1 = 1
					AND d.delivery_status_id = ds.id
				WHERE 1 = 1
				  AND d.is_active = 1
				  AND d.dropshipper_id = ?
				  GROUP BY ds.name) a
				ON 1 = 1
				AND a.delivery_status = ds.name
		`
	}

	if sql == "" {
		return &responseDashboardDeliveryStatus, errors.New("no valid user type to run GetDashboardDeliveryStatus")
	}

	// Run lol
	err := database.DBInstancePublic.Select(
		&responseDashboardDeliveryStatusContainer,
		sql,
		userId,
	)

	if err != nil {
		return &responseDashboardDeliveryStatus, err
	}

	/**
	Transform values
	*/
	for _, v := range responseDashboardDeliveryStatusContainer {
		// responseDashboardDeliveryStatus
		if v.Name == "Proposed" {
			responseDashboardDeliveryStatus.Proposed = v.Amount
		}

		if v.Name == "Accepted" {
			responseDashboardDeliveryStatus.Accepted = v.Amount
		}

		if v.Name == "Fulfilled" {
			responseDashboardDeliveryStatus.Fulfilled = v.Amount
		}

		if v.Name == "Rejected" {
			responseDashboardDeliveryStatus.Rejected = v.Amount
		}

		if v.Name == "Delivered" {
			responseDashboardDeliveryStatus.Delivered = v.Amount
		}
	}

	var (
		DeclaredAmount float64
	)

	// Get total sales
	if userType == "Seller" {
		sql = `
			SELECT 
				IF(SUM(declared_amount) IS NULL, 0, SUM(declared_amount))
			FROM delivery d 
			INNER JOIN delivery_status ds 
				ON 1 = 1
					AND d.delivery_status_id = ds.id 
					AND ds.name = 'Delivered'
			WHERE  1 = 1
				AND seller_id = ? 
				AND is_active = 1
		`

		err := database.DBInstancePublic.Get(&DeclaredAmount, sql, userId)

		if err != nil {
			return &responseDashboardDeliveryStatus, err
		}

		responseDashboardDeliveryStatus.TotalSales = DeclaredAmount
	}

	if userType == "Dropshipper" {
		sql = `
			SELECT 
				IF(SUM(declared_amount) IS NULL, 0, SUM(declared_amount))
			FROM delivery d 
			INNER JOIN delivery_status ds 
				ON 1 = 1
					AND d.delivery_status_id = ds.id 
					AND ds.name IN ('Fulfilled', 'Delivered')
			WHERE  1 = 1
				AND d.dropshipper_id = ? 
				AND is_active = 1
		`

		err := database.DBInstancePublic.Get(&DeclaredAmount, sql, userId)

		if err != nil {
			return &responseDashboardDeliveryStatus, err
		}

		responseDashboardDeliveryStatus.TotalSales = DeclaredAmount
	}

	return &responseDashboardDeliveryStatus, nil

	// Perform different queries whether this is a seller or a dropshipper

}

// GetRates returns the rate provided the declared amount and courier id
func (d *Delivery) GetRates(
	courierId int,
	declaredAmount float64,
) (*ResponseRates, error) {
	// Call stored proc

	/**
	CREATE PROCEDURE `get_service_fee` (
	    IN p_declared_amount INTEGER,
	    IN p_delivery_courier_id INTEGER,
	    OUT _courier_name TEXT,
	    OUT _standard_rate TEXT,
	    OUT _cop_rate TEXT,
	    OUT _cod_rate TEXT
	)
	*/

	// I need a transactions
	tx, err := database.DBInstancePublic.Begin()
	if err != nil {
		return nil, errors.New("error starting transactions at GetRates:" + err.Error())
	}
	// Set variables
	_, err = tx.Exec("SET @courier = 0")
	if err != nil {
		return nil, errors.New("error setting courier_name out at GetRates:" + err.Error())
	}
	_, err = tx.Exec("SET @standard_rate = 0")
	if err != nil {
		return nil, errors.New("error setting standard_rate out at GetRates:" + err.Error())
	}
	_, err = tx.Exec("SET @cop_rate = 0")
	if err != nil {
		return nil, errors.New("error setting cop_rate out at GetRates:" + err.Error())
	}
	_, err = tx.Exec("SET @cod_rate = 0")
	if err != nil {
		return nil, errors.New("error setting cod_rate out at GetRates:" + err.Error())
	}

	sql := `
		CALL get_service_fee(?, ?, @courier, @standard_rate, @cop_rate, @cod_rate)
	`

	// Call proc
	_, err = tx.Exec(sql,
		declaredAmount,
		courierId,
	)
	if err != nil {
		return nil, errors.New("error setting cod_rate out at GetRates:" + err.Error())
	}

	// Return variables
	var courier string
	var standardRate float64
	var copRate float64
	var codRate float64

	// courier_name
	err = tx.QueryRow("SELECT IFNULL(@courier, 0)").Scan(&courier)
	if err != nil {
		return nil, errors.New("error fetching the courier_name on GetRates:" + err.Error())
	}

	err = tx.QueryRow("SELECT IFNULL(@standard_rate, 0)").Scan(&standardRate)
	if err != nil {
		return nil, errors.New("error fetching the standard_rate on GetRates:" + err.Error())
	}

	err = tx.QueryRow("SELECT IFNULL(@cop_rate, 0)").Scan(&copRate)
	if err != nil {
		return nil, errors.New("error fetching the cop_rate on GetRates:" + err.Error())
	}

	err = tx.QueryRow("SELECT IFNULL(@cod_rate, 0)").Scan(&codRate)
	if err != nil {
		return nil, errors.New("error fetching the cod_rate on GetRates:" + err.Error())
	}

	response := ResponseRates{
		Courier:      courier,
		StandardRate: standardRate,
		CopRate:      copRate,
		CodRate:      codRate,
	}

	return &response, err
}
