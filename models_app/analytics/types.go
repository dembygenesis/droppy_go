package analytics

type LineChartTotalSalesItem struct {
	Month  string  `json:"month" db:"month"`
	Amount float64 `json:"amount" db:"amount"`
}

type DeliveryPieChartItem struct {
	DeliveryType  string `json:"delivery_type" db:"delivery_type"`
	DeliveryCount string `json:"delivery_count" db:"delivery_count"`
}

type DeliveryLineChartItem struct {
	DeliveryCount float64 `json:"delivery_count" db:"delivery_count"`
	DeliveryDate  string  `json:"delivery_date" db:"delivery_date"`
}

type AcceptedPercentages struct {
	TotalOrders float64 `json:"total_orders" db:"total_orders"`
	Delivered   float64 `json:"delivered" db:"delivered"`
	Undelivered float64 `json:"undelivered" db:"undelivered"`
	Returned    float64 `json:"returned" db:"returned"`
}
