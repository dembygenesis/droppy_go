package transactions

import (
	"database/sql"
	"droppy_go/database"
	"droppy_go/services/email"
	"droppy_go/services/users"
	"droppy_go/utilities/number"
	"fmt"
	"os"
)

func (t *Transaction) GetAll() (*[]ResponseTransactionList, error) {
	var responseTransactionList []ResponseTransactionList

	sql := `
		SELECT
		  t.id,
		  IF(DATE_FORMAT(CONVERT_TZ(t.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(t.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
		  IF(CONCAT(u.lastname, ', ', u.firstname) IS NULL, '', CONCAT(u.lastname, ', ', u.firstname)) AS created_by,
		  IF(CONCAT(u2.lastname, ', ', u2.firstname) IS NULL, '', CONCAT(u2.lastname, ', ', u2.firstname)) AS updated_by,
		  IF(CONCAT(u3.lastname, ', ', u3.firstname) IS NULL, '', CONCAT(u3.lastname, ', ', u3.firstname)) AS admin_allotted,
		  IF(CONCAT(u4.lastname, ', ', u4.firstname) IS NULL, '', CONCAT(u4.lastname, ', ', u4.firstname)) AS user_allotted,
		  IF(IF(bt.name IS NULL, '', bt.name) IS NULL, '', IF(bt.name IS NULL, '', bt.name)) AS bank,
		  t.is_active,
		  t.reference_number,
		  t.description,
		  t.amount,
		  t.coin_amount,
		  t.money_in
		FROM
		  transaction t
		  INNER JOIN` + "`user`" + `u 
			ON 1 = 1
			  AND t.created_by = u.id
		  LEFT JOIN` + "`user`" + `u2
			ON 1 = 1
			  AND t.updated_by = u2.id
		  INNER JOIN` + "`user`" + `u3 
			ON 1 = 1
			  AND t.admin_allotted_id = u3.id
		  INNER JOIN` + "`user`" + `u4
			ON 1 = 1
			  AND t.user_allotted_id = u4.id
		  INNER JOIN bank_type bt 
			ON 1 = 1
			  AND t.bank_type_id = bt.id
		  
		  ORDER BY t.created_date DESC
	`

	err := database.DBInstancePublic.Select(&responseTransactionList, sql)

	return &responseTransactionList, err
}

func (t *Transaction) Create(p *ParamsTransaction) (*sql.Result, error) {
	sql := `
		CALL add_transaction(?, ?, ?, ?, ?, ?, ?, ?)
	`

	sqlResult, err := database.DBInstancePublic.Exec(
		sql,
		p.Amount,
		p.CoinAmount,
		p.AdminId,
		p.UserId,
		p.MoneyIn,
		p.BankTypeId,
		p.ReferenceNumber,
		p.Description,
	)

	if err != nil {
		return &sqlResult, err
	}

	// Add go routine send email
	go func() {
		userDetail, err := users.GetUserDetails(p.UserId)
		if err != nil {
			fmt.Println("--------------------------err 1", err)
			return
		}

		from := os.Getenv("EMAIL_FROM")
		to := userDetail.Email
		subject := "Coins Added"

		formattedCoinAmount := number.RenderInteger("#,###.##", p.CoinAmount)

		message := "Hi " + userDetail.FirstName + " " + userDetail.LastName + ",<br><br>"
		message += "You have been credited <i>PHP " + formattedCoinAmount + "</i> coins to your account. Happy selling!<br><br>"
		message += "Thanks :),<br/>Droppy"

		attachment := ""
		err = email.SendMail(
			from,
			to,
			subject,
			message,
			attachment,
		)
		if err != nil {
			fmt.Println("--------------------------err 2", err)
		}
	}()

	return &sqlResult, nil
}

func (t *Transaction) Delete(p *ParamsTransactionDelete) (*sql.Result, error) {
	sql := `
		CALL void_transaction(?, ?)
	`

	sqlResult, err := database.DBInstancePublic.Exec(
		sql,
		p.ID,
		p.AdminId,
	)

	return &sqlResult, err
}