package coin_transactions

import (
	"droppy_go/database"
	UtilitiesDate "droppy_go/utilities/date"
	"droppy_go/utilities/response_builder"
	string2 "droppy_go/utilities/string"
)

func (t CoinTransaction) GetAll2(page int, rows int, filters []string, startDate string, endDate string) (*[]ResponseCoinTransactionList, response_builder.Pagination, error) {
	var container []ResponseCoinTransactionList
	var pagination response_builder.Pagination

	// Build filters
	sqlFilters := ""

	// Withdrawal
	if string2.StringInSlice("Withdrawal", filters) {
		sqlFilters += " OR ct.withdrawal_id IS NOT NULL "
	}

	// Disbursement
	if string2.StringInSlice("Disbursement", filters) {
		sqlFilters += " OR ds.name = 'Delivered' "
	}

	// Coins-in
	if string2.StringInSlice("Coins", filters) {
		sqlFilters += " OR ct2.transaction_id IS NOT NULL "
	}

	if sqlFilters != "" {
		sqlFilters = sqlFilters[3:]
	} else {
		sqlFilters = " 1 = 1 "
	}

	// Add date filters
	sqlDateFilter := " 1 = 1 "

	if UtilitiesDate.ValidateDate(startDate) && UtilitiesDate.ValidateDate(endDate) {
		sqlDateFilter = ` ct.created_date BETWEEN "` + startDate + `" AND "` + endDate + `" `
	}

	sql := `
		SELECT
		  ct.id AS coin_transaction_id,
		  IF(DATE_FORMAT(CONVERT_TZ(ct.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(ct.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
		  CONCAT(u.lastname, ', ', u.firstname) AS user_fullname,
		  ut.name AS user_type,
		  ct.type AS credit_or_debit,
		  IF (ct.type = 'D', ABS(ct.amount) * -1, ABS(ct.amount)) AS amount,
		  CASE
			WHEN (ct.order_id IS NOT NULL AND ut.name = "Seller") THEN "Seller Order"
			WHEN (ct.order_id IS NOT NULL AND ut.name = "Dropshipper") THEN "Credit Dropshipper Package Fee"    
			WHEN (ct.delivery_id IS NOT NULL AND ct.amount * - 1 > 0 AND ut.name = "Seller") THEN "Delivery Success"
			WHEN (ct.delivery_id IS NOT NULL AND ct.amount * - 1 < 0 AND ut.name = "Seller") THEN "Delivery Attempt"
			WHEN (ct.delivery_id IS NOT NULL AND ut.name = "Dropshipper" AND ABS(ct.amount) IN (75, 35)) THEN "Credit Dropshipper Handling Fee"    
			WHEN (ct.delivery_id IS NOT NULL AND ut.name = "Dropshipper") THEN "Credit Dropshipper Dropship/Package"
			WHEN (ct.withdrawal_id IS NOT NULL)
			THEN "Withdrawal"
			WHEN (
			  ct.coin_transaction_id IS NOT NULL AND ct.amount * - 1 > 0
			)
			THEN "Money In"
			WHEN (
			  ct.coin_transaction_id IS NOT NULL AND ct.amount * - 1 < 0
			)
			THEN "Money Out"
			ELSE "unknown"
		  END AS transaction_type
		FROM
		  coin_transaction ct
		  INNER JOIN user u
			ON 1 = 1
			AND ct.user_id = u.id
		  INNER JOIN user_type ut
			ON 1 = 1
			AND u.user_type_id = ut.id
		  LEFT JOIN delivery d 
			ON 1 = 1
			AND ct.delivery_id = d.id 
		  LEFT JOIN delivery_status ds
			ON 1 = 1
			AND d.delivery_status_id = ds.id
		  LEFT JOIN coin_transaction ct2 
			ON 1 = 1  
			AND ct.coin_transaction_id = ct2.id
		WHERE 1 = 1
		  AND ct.is_active = 1
		  AND ut.name IN ("Seller", "Dropshipper")
          AND (` + sqlFilters + `)
          AND (` + sqlDateFilter + `)
		ORDER BY ct.created_date DESC
	`

	var err error
	var count int

	paginate := func () (*[]ResponseCoinTransactionList, response_builder.Pagination, error) {
		count, err = database.GetQueryCount(sql)

		if err != nil {
			return &container, pagination, err
		}

		if count == 0 {
			return &container, pagination, nil
		}

		sql, pages, rowsPerPage, offset, page, totalCount, resultCount := database.GetPaginationDetails(
			sql,
			count,
			page,
			rows,
			1000,
		)

		pagination.SetData(rowsPerPage, offset, pages, rows, page, totalCount, resultCount)

		err := database.DBInstancePublic.Select(
			&container,
			sql,
		)

		if err != nil {
			return &container, pagination, err
		}

		return &container, pagination, nil
	}

	res, pagination, err := paginate()

	return res, pagination, err
}

func (t CoinTransaction) GetAll() (*[]ResponseCoinTransactionList, error) {
	var responseTransactionList []ResponseCoinTransactionList

	sql := `
		SELECT
		  ct.id,
          IF(DATE_FORMAT(CONVERT_TZ(ct.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p') IS NULL, "", DATE_FORMAT(CONVERT_TZ(ct.created_date,'+00:00','+08:00'), '%Y-%m-%d %h:%i %p')) AS date_created,
          IF(CONCAT(u2.lastname, ', ', u2.firstname) IS NULL, "", CONCAT(u2.lastname, ', ', u2.firstname)) AS created_by,
          IF(CONCAT(u.lastname, ', ', u.firstname) IS NULL, "", CONCAT(u.lastname, ', ', u.firstname)) AS allotted_to,
          IF(CONCAT(u3.lastname, ', ', u3.firstname) IS NULL, "", CONCAT(u3.lastname, ', ', u3.firstname)) AS updated_by,
          ct.is_active,
          ct.type,
          ct.amount
		FROM
		  coin_transaction ct
		  INNER JOIN user u
			ON 1 = 1
			AND ct.user_id = u.id
		  LEFT JOIN user u2
			ON 1 = 1
			AND ct.created_by = u2.id
		  LEFT JOIN user u3
			ON 1 = 1
			AND ct.updated_by = u3.id
		ORDER BY ct.created_date DESC,
		  ct.type ASC
	`

	err := database.DBInstancePublic.Select(&responseTransactionList, sql)

	return &responseTransactionList, err
}

/*func (t CoinTransaction) Create() (sql.Result, error) {
	var sqlResult sql.Result

	sql := `
		SELECT 5 as hahah
	`

	fmt.Println("sqlResult", sqlResult)

	err := database.DBInstancePublic.Select(&sqlResult, sql)

	return sqlResult, err
}*/