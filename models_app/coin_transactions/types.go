package coin_transactions

// Transaction is an exact map of the schema
type CoinTransaction struct {
	ID int `json:"id" db:"id"`
}

// ResponseCoinTransactionListOld is an exact map of the transaction list query
type ResponseCoinTransactionListOld struct {
	ID          int     `json:"id" db:"id"`
	DateCreated string  `json:"date_created" db:"date_created"`
	CreatedBy   string  `json:"created_by" db:"created_by"`
	AllottedTo  string  `json:"allotted_to" db:"allotted_to"`
	UpdatedBy   string  `json:"updated_by" db:"updated_by"`
	IsActive    int     `json:"is_active" db:"is_active"`
	Type        string  `json:"type" db:"type"`
	Amount      float64 `json:"amount" db:"amount"`
}

type ResponseCoinTransactionList struct {
	CoinTransactionId int     `json:"coin_transaction_id" db:"coin_transaction_id"`
	DateCreated       string  `json:"date_created" db:"date_created"`
	UserFullName      string  `json:"user_fullname" db:"user_fullname"`
	UserType          string  `json:"user_type" db:"user_type"`
	CreditOrDebit     string  `json:"credit_or_debit" db:"credit_or_debit"`
	Amount            float64 `json:"amount" db:"amount"`
	TransactionType   string  `json:"transaction_type" db:"transaction_type"`
}
