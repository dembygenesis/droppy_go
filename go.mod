module droppy_go

go 1.16

require (
	github.com/arsmn/fiber-swagger/v2 v2.17.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/friendsofgo/errors v0.9.2
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofiber/fiber v1.14.6
	github.com/gofiber/fiber/v2 v2.17.0
	github.com/jinzhu/now v1.1.2 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/swaggo/swag v1.7.1
	github.com/volatiletech/null/v8 v8.1.2
	github.com/volatiletech/sqlboiler/v4 v4.6.0
	github.com/volatiletech/strmangle v0.0.1
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
