package coin_transactions

import (
	TransactionController "droppy_go/controllers/transactions"
	UserMiddleware "droppy_go/middlewares/users"
	TransactionMiddleware "droppy_go/middlewares/transactions"
	"github.com/gofiber/fiber/v2"
)

func BindRoutes(api fiber.Router) {

	api = api.Group("/transaction")

	// Crud
	api.Get("/", UserMiddleware.RoleMiddleware("Admin"), TransactionController.GetAll)
	api.Post("/", UserMiddleware.RoleMiddleware("Admin"), TransactionMiddleware.Create, TransactionController.Create)
	api.Delete("/", UserMiddleware.RoleMiddleware("Admin"), TransactionMiddleware.Delete, TransactionController.Delete)
}