package analytics

import (
	AnalyticsController "droppy_go/controllers/analytics"
	UserMiddleware "droppy_go/middlewares/users"
	"github.com/gofiber/fiber/v2"
)

func BindRoutes(api fiber.Router) {

	api = api.Group("/analytics")

	api.Get("/line-chart", UserMiddleware.RoleMiddlewareV2([]string{"Seller"}), AnalyticsController.GetLineChart)
	api.Get("/pie-chart", UserMiddleware.RoleMiddlewareV2([]string{"Seller"}), AnalyticsController.GetPieChart)
	api.Get("/acceptance-percentages", UserMiddleware.RoleMiddlewareV2([]string{"Seller"}), AnalyticsController.GetAcceptancePercentages)
	api.Get("/line-chart-total-sales", UserMiddleware.RoleMiddlewareV2([]string{"Seller"}), AnalyticsController.GetLineChartTotalSales)
}
