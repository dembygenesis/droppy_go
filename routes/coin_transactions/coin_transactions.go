package coin_transactions

import (
	CoinTransactionController "droppy_go/controllers/coin_transactions"
	UserMiddleware "droppy_go/middlewares/users"
	"github.com/gofiber/fiber/v2"
)

func BindRoutes(api fiber.Router) {

	api = api.Group("/coin-transaction")

	// Crud
	api.Get("/", UserMiddleware.RoleMiddleware("Admin"), CoinTransactionController.GetAll)
	api.Post("/", UserMiddleware.RoleMiddleware("Admin"), CoinTransactionController.Create)
}