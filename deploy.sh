# Build go
env GOOS=linux GOARCH=amd64 go build -o droppy_go *.go

# Stop service
ssh droppy "service go-droppy stop"

# Upload file
scp droppy_go droppy:/var/www/droppy_go

# Chmod
ssh droppy "chmod +x /var/www/droppy_go/droppy_go"

# Stop service
ssh droppy "service go-droppy stop"

# Refresh daemons
ssh droppy "systemctl daemon-reload"

# Start service again
ssh droppy "service go-droppy start"
