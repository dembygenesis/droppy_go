package pagination

import (
	"github.com/gofiber/fiber/v2"
	"strconv"
)

func RowsPagesMiddleware(c *fiber.Ctx) error {
	var disabledPagination int
	var page int
	var rows int

	// Set default rows to 100 if not paginated
	if c.Query("page") == "" {
		page = 0
	} else {
		page, _ = strconv.Atoi(c.Query("page"))
	}

	if c.Query("rows") == "" {
		rows = 200
	} else {
		rows, _ = strconv.Atoi(c.Query("rows"))

		if rows <= 0 {
			rows = 50
		} else if rows < 50 {
			rows = 50
		} else if rows > 500 {
			rows = 500
		}
	}

	// Disable pagination default (false)
	if c.Query("disabled_pagination") == "1" {
		disabledPagination = 1
	} else {
		disabledPagination = 0
	}

	c.Locals("disabled_pagination", strconv.Itoa(disabledPagination))
	c.Locals("rows", strconv.Itoa(rows))
	c.Locals("page", strconv.Itoa(page))

	return c.Next()
}