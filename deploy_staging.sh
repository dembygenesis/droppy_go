# Build go
env GOOS=linux GOARCH=amd64 go build -o droppy_go *.go

# Stop service
ssh droppy "service staging-go-droppy stop"

# Remove File
ssh droppy "rm /var/www/staging_droppy_go/droppy_go"

# Upload file
scp droppy_go droppy:/var/www/staging_droppy_go

# Chmod
ssh droppy "chmod +x /var/www/staging_droppy_go/droppy_go"

# Stop service
ssh droppy "service staging-go-droppy stop"

# Refresh daemons 
ssh droppy "systemctl daemon-reload"

# Start service again
ssh droppy "service staging-go-droppy start"
