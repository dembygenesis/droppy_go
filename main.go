package main

import (
	swagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/joho/godotenv"
	"os"

	_ "droppy_go/docs"

	"droppy_go/database"
	AnalyticsRoutes "droppy_go/routes/analytics"
	CoinTransactionRoutes "droppy_go/routes/coin_transactions"
	DeliveryRoutes "droppy_go/routes/deliveries"
	OrderRoutes "droppy_go/routes/orders"
	ProductRoutes "droppy_go/routes/products"
	TransactionRoutes "droppy_go/routes/transactions"
	UserRoutes "droppy_go/routes/users"
	WithdrawalRoutes "droppy_go/routes/withdrawals"
)


// HelloIdiot func for creates a new kiki.
// @Description Create a new kiki.
// @Summary create a new kiki(s)
// @Tags Book
// @Accept json
// @Produce json
// @Param title body string true "Title"
// @Param author body string true "Author"
// @Security ApiKeyAuth
// @Router /v1/kiki [post]
func HelloIdiot() {

}
func Hello() {

}

// @title Fiber Example API
// @version 1.0
// @description This is a sample swagger for FiberV2
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.email fiber@swagger.io
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:8081
// @BasePath /
func main() {
	app := fiber.New(fiber.Config{
		BodyLimit: 20971520,
	})

	// app.Get("/swagger/*", swagger.Handler) // default
	swg := app.Group("/swagger")
	swg.Get("*", swagger.Handler)

	// Initial route group
	api := app.Group("/api/v1", cors.New(), logger.New())

	// Database
	database.EstablishConnection()

	// Routes
	UserRoutes.BindRoutes(api)
	ProductRoutes.BindRoutes(api)
	CoinTransactionRoutes.BindRoutes(api)
	TransactionRoutes.BindRoutes(api)
	OrderRoutes.BindRoutes(api)
	DeliveryRoutes.BindRoutes(api)
	WithdrawalRoutes.BindRoutes(api)
	AnalyticsRoutes.BindRoutes(api)

	// Public routes
	app.Static("/", "./public")
	app.Static("/prefix", "./public")
	app.Static("*", "./public/index.html")

	// Load port from env
	_ = godotenv.Load()

	// Boot app
	_ = app.Listen(":" + os.Getenv("PORT"))
}
